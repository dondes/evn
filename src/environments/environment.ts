// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase : {
    apiKey: "AIzaSyBeMZgkjTPJ4JoU6zL6IA9ssaRkUl7pERE",
    authDomain: "test-database-f9bb5.firebaseapp.com",
    databaseURL: "https://test-database-f9bb5-default-rtdb.europe-west1.firebasedatabase.app",
    projectId: "test-database-f9bb5",
    storageBucket: "test-database-f9bb5.appspot.com",
    messagingSenderId: "442585876289",
    appId: "1:442585876289:web:7b61ccba913f399ded03c0",
    measurementId: "G-0BS3B1H8H7"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
