export const environment = {
  production: true,
  firebase : {
    apiKey: "AIzaSyBeMZgkjTPJ4JoU6zL6IA9ssaRkUl7pERE",
    authDomain: "test-database-f9bb5.firebaseapp.com",
    databaseURL: "https://test-database-f9bb5-default-rtdb.europe-west1.firebasedatabase.app",
    projectId: "test-database-f9bb5",
    storageBucket: "test-database-f9bb5.appspot.com",
    messagingSenderId: "442585876289"
  }
};
