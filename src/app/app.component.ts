import { Component, OnInit, OnDestroy } from '@angular/core';
import { _MatSlideToggleRequiredValidatorModule } from '@angular/material/slide-toggle';
import { ActionService } from './shared/services/action.service';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.less']
})



export class AppComponent implements OnInit, OnDestroy {

  // color = 'primary';
  checked = false;
  disabled = false;
  labelPosition : any = 'before';

  title = 'E.N.V';

  consent = localStorage.getItem('consent');

  isChecked = true;

  consentOptions = [false,false,false];
  
  constructor(
    private as : ActionService
  ) {}

  onToggle($event: any) {
    
    let checker = (arr: any[]) => arr.every((v: boolean) => v === true);

    if($event.source.id === 'mat-slide-toggle-1'){
      this.consentOptions[0] = $event.checked;
    }
    if($event.source.id === 'mat-slide-toggle-2'){
      this.consentOptions[1] = $event.checked;
    }
    if($event.source.id === 'mat-slide-toggle-3'){
      this.consentOptions[2] = $event.checked;
    }
    
    if(checker(this.consentOptions)) {
      this.consent = JSON.stringify(checker(this.consentOptions));
      // localStorage.setItem('consent', this.consent);
    }
  }

  goToConsent(event:any, consent: string): void {
    this.as.openConsentDialog(event, consent);
  }

  ngOnInit(): void {
    // console.log(this.consent);

    // throw new Error('Method not implemented.');
    
  }
  
  ngOnDestroy(): void {
    throw new Error('Method not implemented.');
  }

  agreeConsent() {
    this.consent = 'true';
    // localStorage.setItem('consent', 'akkoord');
  }
  
}
