import { Component, OnInit } from '@angular/core';
import { LocalizationService } from '../shared/services/localization.service';

@Component({
  selector: 'app-language-selection',
  templateUrl: './language-selection.component.html',
  styleUrls: ['./language-selection.component.less']
})
export class LanguageSelectionComponent implements OnInit {
  languages: string[] = ['nl', 'en', 'de', 'fr', 'sp'];

  constructor(private localizationService: LocalizationService) { }

  ngOnInit(): void {
  }

  changeLanguage(language:string): void {
    this.localizationService.setLanguage(language);
  }

}
