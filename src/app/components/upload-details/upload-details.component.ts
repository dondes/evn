import { Component, Input, OnInit } from '@angular/core';

import { FileUploadService } from '../../shared/services/file-upload.service';
import { FileUpload } from '../../shared/models/file-upload.model';

@Component({
  selector: 'app-upload-details',
  templateUrl: './upload-details.component.html',
  styleUrls: ['./upload-details.component.less']
})
export class UploadDetailsComponent implements OnInit {
  @Input() fileUpload!: FileUpload;

  user_data : any;

  constructor(private uploadService: FileUploadService) { }

  ngOnInit(): void {

    this.user_data = JSON.parse(sessionStorage.user);
  }

  deleteFileUpload(fileUpload: FileUpload): void {
    this.uploadService.deleteFile(this.user_data?.uid, fileUpload);
  }

}
