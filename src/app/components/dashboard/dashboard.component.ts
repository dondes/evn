import { Component, OnInit, Inject } from '@angular/core';
import { AuthService } from "../../shared/services/auth.service";
import { DOCUMENT } from '@angular/common';
import { UserDataService } from 'src/app/shared/services/user-data.service';
import { IsLoadingService } from 'src/app/shared/services/is-loading.service';


@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.less']
})

export class DashboardComponent implements OnInit {
  allComplete: boolean = false;
  localData: any;


  // // UserData array
  user_data: any;
  
  constructor(
    public authService: AuthService, 
    public userDataService: UserDataService,
    private isLoadingService: IsLoadingService,
    @Inject(DOCUMENT) public _document: Document
  ) { }

  async ngOnInit(): Promise<void> {
    await this.fakeAsyncCallToFetchItems();
  }

  async getUserData(uid:string) {
    this.userDataService.getUser(uid).valueChanges().subscribe(val => {
      this.user_data = val;
    });
  }

  async fakeAsyncCallToFetchItems() {
    await this.isLoadingService.add(wait(1500), {
      key: "async-loading"
    });

    if(sessionStorage.getItem('user')!.length === 9){
      this._document.defaultView?.location.reload();
    }

    let userDData = JSON.parse(sessionStorage.user);

    this.user_data = this.getUserData(userDData.uid);

  }
}

export function wait(nm: number) {
  return new Promise((res) => {
    setTimeout(res, nm);
  });
}