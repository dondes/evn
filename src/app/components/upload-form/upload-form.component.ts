import { Component, OnInit } from '@angular/core';
import { FileUploadService } from '../../shared/services/file-upload.service';
import { FileUpload } from '../../shared/models/file-upload.model';

@Component({
  selector: 'app-upload-form',
  templateUrl: './upload-form.component.html',
  styleUrls: ['./upload-form.component.less']
})
export class UploadFormComponent implements OnInit {
  aFiles: number = 0;
  selectedFiles?: FileList;
  currentFileUpload?: FileUpload;
  percentage = 0;
  user_data : any ;
  
  constructor(private uploadService: FileUploadService) { }

  ngOnInit(): void {
    this.user_data = JSON.parse(sessionStorage.user);
  }

  selectFile(event: any): void {
    this.selectedFiles = event.target.files;
    this.aFiles = this.selectedFiles!.length;
  }

  upload(): void {
    for (let i = 0; i <= this.selectedFiles!.length ; i++) {
      const file: File | null = this.selectedFiles!.item(i);
      // this.selectedFiles = undefined;

      if (file) {
        this.currentFileUpload = new FileUpload(file);
        this.uploadService.pushFileToStorage(this.user_data.uid, this.currentFileUpload).subscribe(
          percentage => {
            this.percentage = Math.round(percentage ? percentage : 0);
          },
          error => {
            console.log(error);
          }
        );
      }
    }

    this.selectedFiles = undefined;
  }
}
