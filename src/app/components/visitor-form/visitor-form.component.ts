import { DOCUMENT } from '@angular/common';
import { Component, Inject, OnInit } from '@angular/core';
import { FileUploadService } from 'src/app/shared/services/file-upload.service';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { UserDataService } from '../../shared/services/user-data.service';
import { userReviewsExtended } from 'src/app/shared/services/user';
import { AuthService } from "../../shared/services/auth.service";
import { EmployeeDetailsComponent } from '../employee-details/employee-details.component';

import { map } from 'rxjs/operators';

@Component({
  selector: 'app-visitor-form',
  templateUrl: './visitor-form.component.html',
  styleUrls: ['./visitor-form.component.less']
})
export class VisitorFormComponent implements OnInit {
  fileUploads?: any[];
  firstFileUrl?: string;

  // UserData array
  user_data: any;
  localData: any;
  curUserData: any;

  // Review's array
  userReviews!: userReviewsExtended[];

  newReviews: any[] = [];

  userRatings: any[] = [];

  userReviewers: any[] = [];

  scorePercentage: any[] = [];

  average: any;

  selectedIndex!: number;

  averagePercentage: any;

  constructor(
    public authService: AuthService,
    public userDataService: UserDataService,
    private uploadService: FileUploadService,
    public dialog: MatDialog,
    public dialogRef: MatDialogRef<EmployeeDetailsComponent>,
    @Inject(DOCUMENT) public _document: Document
  ) { }

  async ngOnInit(): Promise<void> {

    await this.getUserData();

    await this.getReviews();

    for (let i = 0; i < this.userReviews.length; i++) {
      this.userReviews[i].revs = this.userReviews[i].revs.sort((a: { name: string; }, b: { name: string; }) => a.name.localeCompare(b.name));
    }

    this.localData = JSON.parse(sessionStorage.user);

    this.uploadService.getFiles(this.localData?.uid, 20).snapshotChanges().pipe(
      map((changes) =>
        // store the key
        changes.map(c => ({ key: c.payload.key, ...c.payload.val() }))
      )
    ).subscribe(fileUploads => {
      this.fileUploads = fileUploads;
      this.firstFileUrl = (this.fileUploads.length !== 0 ? this.fileUploads[0].url : '../../../assets/profile.svg');
    });
  }

  async getUserData() {
    let userDData;
    if (sessionStorage.getItem('user')!.length === 9) {
      this._document.defaultView?.location.reload();
    }
    userDData = JSON.parse(sessionStorage.user);
    this.userDataService.getUser(userDData.uid).valueChanges().subscribe(val => {
      this.user_data = val;
    });
  }

  async getReviews() {

    this.userReviews = [];
    this.userDataService.getAllData().snapshotChanges().pipe(
      map(changes =>
        changes.map(c =>
          ({ id: c.payload.doc.id, ...c.payload.doc.data() })
        )
      )
    ).subscribe(users => {
      users.forEach(usr => {
        var uid = usr.uid;
        this.userDataService.getUserReviews(uid).valueChanges()
          .subscribe(docs => {
            if (docs)
              docs.forEach(doc => {
                if (doc)
                  // console.log(doc);
                var rev = JSON.parse(JSON.stringify(doc));
                this.userDataService.getUser(uid).valueChanges().subscribe(usr => {
                  if (usr) {
                    // console.log(usr);
                    var review: userReviewsExtended = {
                      displayName: usr.displayName,
                      revs: rev && rev.revs ? rev.revs : [],
                      uid: {
                        reviewer: usr.uid,
                        review: rev && rev.review ? rev.review : '',
                        score: rev && rev.score ? rev.score : 0,
                        datum: rev && rev.datum ? rev.datum : 0,
                        hygene: rev && rev.hygene ? rev.hygene : 0,
                        location: rev && rev.location ? rev.location : 0,
                      }
                    };

                    this.userReviews.push(review);
                    this.scorePercentage.push(20 * (rev && rev.score ? rev.score : 0) + '%');
                  }
                })
              }
              );

          });
      })
    });
    this.userReviews = this.userReviews.sort((a, b) => {
      return <any>new Date(b.uid.datum) - <any>new Date(a.uid.datum);
    })
  }

  async onSubmit() {
    this.curUserData = JSON.parse(sessionStorage.user);
    // this.userDataService.form.value.user_data = this.user_data;
    let data = this.userDataService.form1.value;
    // console.log(data);

    if (data.displayName.length === 0) {
      data.displayName = (this.user_data.displayName.length == 0) ? '' : this.user_data.displayName;
    } else {
      this.curUserData.displayName = data.displayName;
    }

    sessionStorage.clear();
    sessionStorage.setItem('user', JSON.stringify(this.curUserData));
    // console.log(data);
    await this.updateReviews(this.userReviews, data.displayName);

    await this.userDataService.updateUserDataReviewer(data, this.curUserData.uid);

    this.refreshPage();
  }

  openDialog1(data: any): void {
    this.dialog.open(data);
  }

  selectedReview(data: number) {
    this.selectedIndex = (this.selectedIndex !== data) ? data : -9999;
  }

  async updateReviews(reviews: any, reviewer: any) {
    if(reviews.length !== 0 || reviews.length !== undefined) {
      // console.log(reviews);
      for(let i = 0; i < reviews.length; i++) {
        console.log(i + ' ---> ' +reviews[i].uid.datum);
        this.userDataService.updateUserReviews(reviews[i].uid.datum, reviews[i].uid.reviewer, reviewer);
      }
    }
    
  }

  async deleteReview(reviewdUid: any, dateReviewd: any) {
    this.userDataService.deleteReview(reviewdUid, dateReviewd);

    await this.userDataService.delay(500);

    this.ngOnInit();
  }

  refreshPage() {
    this._document.defaultView?.location.reload();
  }
}