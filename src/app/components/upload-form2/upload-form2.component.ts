import { Component, OnInit } from '@angular/core';
import { FileUpload2Service } from '../../shared/services/file-upload2.service';
import { FileUpload } from '../../shared/models/file-upload.model';
import { VideoProcessingService } from 'src/app/shared/services/video-processing-services';
import { ThisReceiver } from '@angular/compiler';

@Component({
  selector: 'app-upload-form2',
  templateUrl: './upload-form2.component.html',
  styleUrls: ['./upload-form2.component.less']
})
export class UploadForm2Component implements OnInit {
  aFiles: number = 0;
  selectedFiles?: FileList;
  currentFileUpload?: FileUpload;
  percentage = 0;
  user_data : any ;
  thumbnailData: any ;

  constructor(private uploadService: FileUpload2Service) { }

  ngOnInit(): void {
    this.user_data = JSON.parse(sessionStorage.user);
    
  }

  selectFile(event: any): void {
    let tempSelectedFiles = event.target.files;
    for(let i = 0; i < tempSelectedFiles.length; i++){
      // if(tempSelectedFiles![i].size > 7340032) {
        
      if(tempSelectedFiles![i].size > 48700160) {
        alert(tempSelectedFiles![i].name +' is te groot kan niet worden geupload, bestanden mogen maximaal 7MB zijn');
      } else {
        this.selectedFiles = event.target.files;
        this.aFiles = this.selectedFiles!.length;
      
        // this.videoService.generateThumbnail(this.selectedFiles![i]).then(thumbnailData => {
        //   this.thumbnailData = thumbnailData;
        // })
      }
      
    }
  }

  upload(): void {
    for (let i = 0; i <= this.selectedFiles!.length ; i++) {
      const file: File | null = this.selectedFiles!.item(i);
      this.selectedFiles = undefined;

      if (file) {
        this.currentFileUpload = new FileUpload(file);
        this.uploadService.pushFileToStorage(this.user_data.uid, this.currentFileUpload).subscribe(
          percentage => {
            this.percentage = Math.round(percentage ? percentage : 0);
          },
          error => {
            console.log(error);
          }
        );
      }
    }

    this.selectedFiles = undefined;
  }
}