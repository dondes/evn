import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CookiesStatementComponent } from './cookies-statement.component';

describe('CookiesStatementComponent', () => {
  let component: CookiesStatementComponent;
  let fixture: ComponentFixture<CookiesStatementComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CookiesStatementComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CookiesStatementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
