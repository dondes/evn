import { Component, OnInit } from '@angular/core';
import { ActionService } from 'src/app/shared/services/action.service';

@Component({
  selector: 'app-cookies-statement',
  templateUrl: './cookies-statement.component.html',
  styleUrls: ['./cookies-statement.component.less']
})
export class CookiesStatementComponent implements OnInit {

  constructor(private as : ActionService) { }

  ngOnInit(): void {
  }

  goToConsent(event:any, consent: string): void {
    this.as.openConsentDialog(event, consent);
  }

}
