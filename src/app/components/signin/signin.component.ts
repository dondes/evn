import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { AuthService } from "../../shared/services/auth.service";

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.less']
})
export class SigninComponent implements OnInit {
  hide = true;

  constructor(private _location: Location, public authService: AuthService) { }

  ngOnInit(): void {
    this.authService.isLoggedIn;
  }
  
  backClicked() {
    this._location.back();
  }
}
