import { Component, OnInit } from '@angular/core';
import { FileUpload2Service } from '../../shared/services/file-upload2.service';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-upload-list2',
  templateUrl: './upload-list2.component.html',
  styleUrls: ['./upload-list2.component.less']
})
export class UploadList2Component implements OnInit {
  fileUploads?: any[];
  user_data: any;

  constructor(private uploadService: FileUpload2Service) { }

  ngOnInit(): void {
    this.user_data = JSON.parse(sessionStorage.user);

    this.uploadService.getFiles(this.user_data?.uid, 20).snapshotChanges().pipe(
      map(changes =>
        // store the key
        changes.map(c => ({ key: c.payload.key, ...c.payload.val() }))
      )
    ).subscribe(fileUploads => {
      this.fileUploads = fileUploads;
    });
  }

}
