import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import { NgbCarousel } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-dialog-body',
  templateUrl: './dialog-body.component.html',
  styleUrls: ['./dialog-body.component.less']
})
export class DialogBodyComponent implements OnInit {
  images! : [];
  interval = 5000;
  showNavigationArrows = true;
  showNavigationIndicators = true;
  pauseOnHover = false;
  activeSlide! : string;
  wrap = true;
  keyboard = true;
  touch = true;
  
  @ViewChild('carousel', { static: true }) carousel!: NgbCarousel;


  constructor(@Inject(MAT_DIALOG_DATA) private data: any, public dialogRef: MatDialogRef<DialogBodyComponent>) {
    // customize default values of carousels used by this component tree
  }

  ngOnInit(): void {
    this.images = this.data[0];
    this.activeSlide = this.data[1];
  }

  close() {
    this.dialogRef.close();
  }
}
