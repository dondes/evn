import { Component, Input, OnInit } from '@angular/core';

import { FileUpload2Service } from '../../shared/services/file-upload2.service';
import { FileUpload } from '../../shared/models/file-upload.model';

@Component({
  selector: 'app-upload-details2',
  templateUrl: './upload-details2.component.html',
  styleUrls: ['./upload-details2.component.less']
})
export class UploadDetails2Component implements OnInit {
  @Input() fileUpload!: FileUpload;

  user_data : any;

  constructor(private uploadService: FileUpload2Service) { }

  ngOnInit(): void {

    this.user_data = JSON.parse(sessionStorage.user);
  }

  deleteFileUpload(fileUpload: FileUpload): void {
    this.uploadService.deleteFile(this.user_data?.uid, fileUpload);
  }

}
