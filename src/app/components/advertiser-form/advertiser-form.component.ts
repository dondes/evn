import { Component, Inject, OnInit } from '@angular/core';
import { UserDataService } from '../../shared/services/user-data.service';
import { FileUploadService } from '../../shared/services/file-upload.service';
import { FileUpload2Service } from 'src/app/shared/services/file-upload2.service';
import { DOCUMENT, Location } from '@angular/common';
import { map } from 'rxjs/operators';
import { Editor, Toolbar } from 'ngx-editor';


@Component({
  selector: 'app-advertiser-form',
  templateUrl: './advertiser-form.component.html',
  styleUrls: ['./advertiser-form.component.less']
})
export class AdvertiserFormComponent implements OnInit {

  editor: Editor = new Editor ;

  toolbar: Toolbar = [
    ["bold", "italic"],
    ["underline", "strike"],
    ["ordered_list", "bullet_list"],
  ];

  allComplete: boolean = false;
  videoUploads?: any[];
  fileUploads?: any[];
  firstFileUrl?: string;

  infoText = '';

  // provincies en gemeentes
  prov_gems = this.userDataService.prov_gems;

  // gender
  gender = this.userDataService.gender;

  // breast size
  breast_size = this.userDataService.breast_size;

  // origins
  origins = this.userDataService.origins;

  // Languages
  languages = this.userDataService.languages;

  // hair colors
  hair_color = this.userDataService.hair_color;

  // Body types
  body_type = this.userDataService.body_type;

  // Date preferences
  date_preference = this.userDataService.date_preference;

  // Sexual preferences
  sexualPref = this.userDataService.sexualPref;

  // Services
  dateServices = this.userDataService.dateServices;

  // Services extra
  // dateServicesExtra = this.userDataService.dateServicesExtra;

  // Prices
  prices_po = this.userDataService.prices_po;
  
  prices_es = this.userDataService.prices_es;
  
  // Availability
  availability = this.userDataService.availability;
  
  // UserData array
  user_data: any;

  selected: string = '';

  curUserData: any;

  genderContent: string = '';

  dateTypeContent: string = '';

  constructor(
    private location: Location, 
    private uploadService: FileUploadService, 
    private uploadService2: FileUpload2Service, 
    public userDataService: UserDataService,
    @Inject(DOCUMENT) public _document: Document
  ) { }

  async ngOnInit(): Promise<void> {

    this.dateServices = this.dateServices.sort((a: { name: string; }, b: { name: string; }) => a.name !== b.name ? a.name < b.name ? -1 : 1 : 0);
    
    await this.getUserData();
    
    this.userDataService.uid = this.user_data?.uid;
  }

  async onSubmit() {

    this.curUserData = JSON.parse(sessionStorage.user);
    // this.userDataService.form.value.user_data = this.user_data;
    let data = this.userDataService.form.value;
    
    if(data.Maandag.hd === false ) {
      if((data.Maandag.bt.length == 0) && (data.Maandag.et.length == 0)){
        data.Maandag.nb = true;
        data.Maandag.bt = '';
        data.Maandag.et = '';
      }
    } else {
      data.Maandag.nb = false;
      data.Maandag.bt = '';
      data.Maandag.et = '';
    }

    if(data.Dinsdag.hd === false ) {
      if((data.Dinsdag.bt.length == 0) && (data.Dinsdag.et.length == 0)){
        data.Dinsdag.nb = true;
        data.Dinsdag.bt = '';
        data.Dinsdag.et = '';
        data.Dinsdag.hd = false;
      }
    } else {
      data.Dinsdag.nb = false;
      data.Dinsdag.bt = '';
      data.Dinsdag.et = '';
    }
    
    if(data.Woensdag.hd === false ) {
      if((data.Woensdag.bt.length == 0) && (data.Woensdag.et.length == 0)){
        data.Woensdag.nb = true;
        data.Woensdag.bt = '';
        data.Woensdag.et = '';
        data.Woensdag.hd = false;
      }
    } else {
      data.Woensdag.nb = false;
      data.Woensdag.bt = '';
      data.Woensdag.et = '';
    }
    
    if(data.Donderdag.hd === false ) {
      if((data.Donderdag.bt.length == 0) && (data.Donderdag.et.length == 0)){
        data.Donderdag.nb = true;
        data.Donderdag.bt = '';
        data.Donderdag.et = '';
        data.Donderdag.hd = false;
      }
    } else {
      data.Donderdag.nb = false;
      data.Donderdag.bt = '';
      data.Donderdag.et = '';
    }

    if(data.Vrijdag.hd === false ) {
      if((data.Vrijdag.bt.length == 0) && (data.Vrijdag.et.length == 0)){
        data.Vrijdag.nb = true;
        data.Vrijdag.bt = '';
        data.Vrijdag.et = '';
        data.Vrijdag.hd = false;
      }
    } else {
      data.Vrijdag.nb = false;
      data.Vrijdag.bt = '';
      data.Vrijdag.et = '';
    }

    if(data.Zaterdag.hd === false ) {
      if((data.Zaterdag.bt.length == 0) && (data.Zaterdag.et.length == 0)){
        data.Zaterdag.nb = true;
        data.Zaterdag.bt = '';
        data.Zaterdag.et = '';
        data.Zaterdag.hd = false;
      }
    } else {
      data.Zaterdag.nb = false;
      data.Zaterdag.bt = '';
      data.Zaterdag.et = '';
    }

    if(data.Zondag.hd === false ) {
      if((data.Zondag.bt.length == 0) && (data.Zondag.et.length == 0)){
        data.Zondag.nb = true;
        data.Zondag.bt = '';
        data.Zondag.et = '';
        data.Zondag.hd = false;
      }
    } else {
      data.Zondag.nb = false;
      data.Zondag.bt = '';
      data.Zondag.et = '';
    }
    
    if (data.displayName.length === 0) {
      data.displayName = (this.user_data.displayName.length == 0)? '' : this.user_data.displayName;
    } else {
      this.curUserData.displayName = data.displayName;
    }
    console.log(this.user_data);
    if (data.provGem.gem.length === 0) {
      data.provGem = this.user_data.provGem;
    } else {
      
      // split gem form data
      var splitted = data.provGem.gem.split(";",2);
      // change form prov_data data to splitted data
      data.provGem.gem = splitted[0];
      data.provGem.prov = splitted[1];
      
      
      // add form data to curUserData
      this.curUserData.provGem = data.provGem;
    }

    if (data.introTitle.length === 0) {
      data.introTitle = this.user_data.introTitle;
    } else {
      this.curUserData.introTitle = data.introTitle;
    }

    if (this.infoText.length === 0) {
      data.info = this.user_data.info;
    } else {
      this.curUserData.info = this.infoText;
    }

    if (data.age === 0) {
      data.age = this.user_data.age;
    } else {
      this.curUserData.age = data.age;
    }

    if (data.origin.length === 0) {
      data.origin = this.user_data.origin;
    } else {
      this.curUserData.origin = data.origin;
    }

    if (data.hairColor.length === 0) {
      data.hairColor = this.user_data.hairColor;
    } else {
      this.curUserData.hairColor = data.hairColor;
    }

    if (data.bodyType.length === 0) {
      data.bodyType = this.user_data.bodyType;
    } else {
      this.curUserData.bodyType = data.bodyType;
    }

    if (data.sexualPref.length === 0) {
      data.sexualPref = this.user_data.sexualPref;
    } else {
      this.curUserData.sexualPref = data.sexualPref;
    }

    if (data.lengte.length === 0) {
      data.lengte = this.user_data.lengte;
    } else {
      this.curUserData.lengte = data.lengte;
    }

    if (data.mobNumber.length === 0) {
      data.mobNumber = this.user_data.mobNumber;
    } else {
      this.curUserData.mobNumber = data.mobNumber;
    }

    if (data.gender.length === 0) {
      data.gender = this.user_data.gender;
    } else {
      this.curUserData.gender = data.gender;
    }

    data.accType = this.user_data.accType;
    data.childNum = 0;

    sessionStorage.clear();
    sessionStorage.setItem('user', JSON.stringify(this.curUserData));
    
    await this.userDataService.updateUserData(data, this.curUserData.uid, Date.now());
    // this.userDataService.createUserData(data).then(res =>{
    //       /*do something here....
    //          maybe clear the form or give a success message*/
    // });



    // this.refreshPage();
  }

  radioButtonChanged($event: { source: { name: any; }; value: string; }) {
    // if you need the event source and value it can be accessed.
    // console.log($event.source.name, '---'+$event.value);

    switch ($event.value) {
      case 'Female':
        this.genderContent = $event.value;
        this.userDataService.clearInputMethod();
        break;
      case 'Male':
        this.genderContent = $event.value;
        this.userDataService.clearInputMethod();
        break;
      case 'Shemale':
        this.genderContent = $event.value;
        this.userDataService.clearInputMethod();
        break;
      case 'Couple':
        this.genderContent = $event.value;
        this.userDataService.clearInputMethod();
        break;
    }
  }

  checkButtonChanged($event: { source: { name: any; }; value: string; }) {
    switch ($event.value) {
      case 'es':
        this.dateTypeContent = $event.value;

        break;
      case 'po':
        this.dateTypeContent = $event.value;
        break;
    }
  }

  async getUserData() {
    let userDData = JSON.parse(sessionStorage.user);
    
    if(sessionStorage.getItem('user')!.length === 9){
      this._document.defaultView?.location.reload();
    }

    this.userDataService.getUser(userDData.uid).valueChanges().subscribe(val => {
      this.user_data = val;
      this.infoText = this.user_data.info;
      // this.genderContent = this.user_data.gender;
      console.log(this.user_data);
    });

    this.uploadService.getFiles(userDData.uid, 20).snapshotChanges().pipe(
      map(changes =>
        // store the key
        changes.map(c => ({ key: c.payload.key, ...c.payload.val() }))
      )
    ).subscribe(fileUploads => {
      this.fileUploads = fileUploads;
      this.firstFileUrl = (this.fileUploads.length !== 0 ? this.fileUploads[0].url : '../../../assets/profile.svg') ;
    });

    this.uploadService2.getFiles(userDData.uid, 20).snapshotChanges().pipe(
      map(changes =>
        // store the key
        changes.map(c => ({ key: c.payload.key, ...c.payload.val() }))
      )
    ).subscribe(fileUploads => {
      this.videoUploads = fileUploads;
      // console.log(this.videoUploads);
    });
  }

  refreshPage() {
    this.curUserData = JSON.parse(sessionStorage.user);
    this._document.defaultView?.location.reload();
  }
}