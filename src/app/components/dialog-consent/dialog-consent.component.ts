import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import { NgbCarousel } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-dialog-consent',
  templateUrl: './dialog-consent.component.html',
  styleUrls: ['./dialog-consent.component.less']
})
export class DialogConsentComponent implements OnInit {
  consents! : [];
  interval = 0;
  showNavigationArrows = false;
  showNavigationIndicators = true;
  pauseOnHover = false;
  activeSlide! : string;
  wrap = true;
  keyboard = true;
  touch = true;

  @ViewChild('carousel', { static: true }) carousel!: NgbCarousel;

  constructor(
    @Inject(MAT_DIALOG_DATA) private data: any, 
    public dialogRef: MatDialogRef<DialogConsentComponent>
  ) { }

  ngOnInit(): void {
    this.consents = this.data[0];
    this.activeSlide = this.data[1];
  }

  close() {
    this.dialogRef.close();
  }

}
