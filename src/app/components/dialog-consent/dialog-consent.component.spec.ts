import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogConsentComponent } from './dialog-consent.component';

describe('DialogConsentComponent', () => {
  let component: DialogConsentComponent;
  let fixture: ComponentFixture<DialogConsentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DialogConsentComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogConsentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
