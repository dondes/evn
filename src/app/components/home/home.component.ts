import { Component, OnInit } from '@angular/core';
import { UserDataService } from 'src/app/shared/services/user-data.service';
import { map } from "rxjs/operators";
import { Subscription } from 'rxjs';
import { FileUploadService } from '../../shared/services/file-upload.service';
import { FileUpload2Service } from '../../shared/services/file-upload2.service';
import { ActionService, FilterData } from 'src/app/shared/services/action.service';
import { AllUsers } from 'src/app/shared/services/user';
import { IsLoadingService } from 'src/app/shared/services/is-loading.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.less']
})

export class HomeComponent implements OnInit {
  
  constructor(
    private userDataService: UserDataService,
    private uploadService: FileUploadService,
    private uploadService2: FileUpload2Service,
    private actionService: ActionService,
    private isLoadingService: IsLoadingService,
  ) { }

  originFilter: any[] = [];
  genderFilter: any[] = [];
  hairFilter: any[] = [];
  bodyFilter: any[] = [];
  ageFilter: number = 0;
  languageFilter: any = [];
  servicesFilter: any = [];

  reviewCalcObjects: { [key: string]: any } = [];

  autoTicks = true;
  disabled = false;
  invert = false;
  max = 100;
  min = 21;
  start = 100;
  showTicks = true;
  step = 1;
  thumbLabel = true;
  value = 0;
  vertical = false;
  tickInterval = 1;

  users?: any[];

  allUsers!: AllUsers[];

  origAllUsers!: AllUsers[];

  prefs: any = [];

  page!: number;

  userName!: string;

  searchTerm!: string;

  siteImg = "../../../assets/profile.svg";

  subscription: Subscription = new Subscription;
  userSource: Subscription = new Subscription;

  showView: any = {};

  status = false;

  toggleClass: string = "closed";

  options = FilterData;

  gender: any = this.options[0].gender;

  origin: any = this.options[0].origins;

  hair_color: any = this.options[0].hair_color;

  body_type: any = this.options[0].body_type;

  language: any = this.options[0].languages;

  services: any = this.options[0].dateServices;

  userImgFilesLenght: { [key: string]: any } = [];

  userVidFilesLenght: { [key: string]: any } = [];

  // age: any;

  genderFiltered: any[] = [];

  hairFiltered: any[] = [];

  bodyFiltered: any[] = [];

  languageFiltered: any[] = [];

  originFiltered: any[] = [];

  servicesFiltered: any[] = [];

  message!: string;

  gridView = false;

  rating: any;

  // public showView2: string = 'grid';

  searchView = false;

  public showSearch: string = 'closed';


  public get filteredData(): any[] {
    let filtered = [...this.origAllUsers];

    if (this.genderFilter.length > 0) {
      filtered = filtered.filter(x => {
        const p = this.genderFilter.find(v => v.id === x.gender)
        return p != null;
      });
    }

    if (this.originFilter.length > 0) {
      filtered = filtered.filter(x => {
        const t = this.originFilter.find(v => v.id === x.origin);
        return t != null;
      });
    }

    if (this.hairFilter.length > 0) {
      filtered = filtered.filter(x => {
        const t = this.hairFilter.find(v => v.id === x.hairColor);
        return t != null;
      });
    }

    if (this.bodyFilter.length > 0) {
      filtered = filtered.filter(x => {
        const t = this.bodyFilter.find(v => v.id === x.bodyType);
        return t != null;
      });
    }

    if (this.languageFilter.length > 0) {
      filtered = filtered.filter(x => {
        const languagesOfUser = Object.entries(x.languages).map(([k, v]) => ({ [k]: v }));
        return this.languageFilter.filter((f: any) => { return languagesOfUser.find(d => d[f.code] == true) }).length;
      });
    }

    if (this.servicesFilter.length > 0) {
      filtered = filtered.filter(x => {
        const servicesOfUser = Object.entries(x.dateServices).map(([k, v]) => ({ [k]: v }));
        return this.servicesFilter.filter((f: any) => { return servicesOfUser.find(d => d[f.code] == true) }).length;
      });
    }

    if (this.ageFilter) {
      filtered = filtered.filter(x => x.age <= this.ageFilter && x.age >= 21);
    }

    return filtered;
  }

  async ngOnInit(): Promise<void> {

    this.options[0].dateServices = this.options[0].dateServices.sort((a: { name: string; }, b: { name: string; }) => a.name !== b.name ? a.name < b.name ? -1 : 1 : 0);
    // console.log(localStorage);
    // console.log(this.options)

    // console.log(this.servicesFiltered.length);

    await this.fakeAsyncCallToFetchItems();

    let tempVar = sessionStorage.getItem('View');

    if (tempVar?.length === 0 || tempVar?.length === undefined) {
      sessionStorage.setItem('View', 'grid');
    }

    this.showView.text = sessionStorage.getItem('View');

    this.gridView = (this.showView.text === 'list' ? true : false);

    let tempPage = sessionStorage.getItem('Activepage');

    if (tempPage?.length === 0 || tempPage?.length === undefined) {
      sessionStorage.setItem('Activepage', '1');
    }

    this.page = +sessionStorage.getItem('Activepage')!;

    this.genderFilter = (sessionStorage?.genderFilter ? JSON.parse(sessionStorage.genderFilter) : '');
    this.genderFiltered = (sessionStorage?.genderFiltered ? JSON.parse(sessionStorage.getItem('genderFiltered')!) : '');

    this.value = (sessionStorage.ageFilter ? JSON.parse(sessionStorage.ageFilter) : 100);

    this.hairFilter = (sessionStorage?.hairFilter ? JSON.parse(sessionStorage.hairFilter) : '');
    this.hairFiltered = (sessionStorage?.hairFiltered ? JSON.parse(sessionStorage.getItem('hairFiltered')!) : '');

    this.bodyFilter = (sessionStorage?.bodyFilter ? JSON.parse(sessionStorage.bodyFilter) : '');
    this.bodyFiltered = (sessionStorage?.bodyFiltered ? JSON.parse(sessionStorage.getItem('bodyFiltered')!) : '');

    this.languageFilter = (sessionStorage?.languageFilter ? JSON.parse(sessionStorage.languageFilter) : '');
    this.languageFiltered = (sessionStorage?.languageFiltered ? JSON.parse(sessionStorage.getItem('languageFiltered')!) : '');

    this.originFilter = (sessionStorage?.originFilter ? JSON.parse(sessionStorage.originFilter) : '');
    this.originFiltered = (sessionStorage?.originFiltered ? JSON.parse(sessionStorage.getItem('originFiltered')!) : '');

    this.servicesFilter = (sessionStorage?.servicesFilter ? JSON.parse(sessionStorage.servicesFilter) : '');
    this.servicesFiltered = (sessionStorage?.servicesFiltered ? JSON.parse(sessionStorage.getItem('servicesFiltered')!) : '');

    this.subscription = this.actionService.getClass().subscribe(showView => {

      this.showView = showView;

      sessionStorage.setItem('View', this.showView.text);
    });

    this.userSource = this.actionService.getMessage().subscribe(userName => this.userName = userName);
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  async getAllUsersData(): Promise<void> {
    this.userDataService.getAllData().snapshotChanges().pipe(
      map(changes =>
        changes.map(c =>
          ({ id: c.payload.doc.id, ...c.payload.doc.data() })
        )
      )
    ).subscribe(data => {
      this.users = data;
      // this.allUsers = this.users;
      this.allUsers = this.users.filter(obj => {
        return obj.published === true && obj.accType !== '2';
      });

      this.users.forEach((user) => this.userDataService.getUserReviews(user.uid)
        .get().pipe().subscribe(snap => {
          snap.forEach(doc => {
            var reviewCalcObject = this.reviewCalcObjects[user.uid];

            if (reviewCalcObject) {
              // console.log(this.reviewCalcObjects);
              reviewCalcObject.totalscore = reviewCalcObject.totalscore + doc.data().score;
              reviewCalcObject.count = reviewCalcObject.count + 1;
              reviewCalcObject.average = reviewCalcObject.totalscore / reviewCalcObject.count * 20; //20%==1 punt
              reviewCalcObject.averageString = reviewCalcObject.average.toString() + '%'; //20%==1 punt
            } else {

              this.reviewCalcObjects[user.uid] = {
                name: user.uid,
                totalscore: doc.data().score,
                count: 1,
                average: doc.data().score * 20,
                averageString: (doc.data().score * 20).toString() + '%',
              };
            }
          })
        })
      );

      this.origAllUsers = this.allUsers;
      this.getUsersFiles();
      this.getUserPrefs();

      this.userDataService.allUsers = this.allUsers;
    });
  }

  async fakeAsyncCallToFetchItems() {
    await this.isLoadingService.add(wait(1500), {
      key: "async-loading"
    });

    return await this.getAllUsersData();
  }

  async getUsersFiles(): Promise<void> {
    if (this.allUsers != undefined) {
      for (let i = 0; i < this.allUsers?.length; i++) {
        let uid = this.allUsers?.[i].uid;
        let filesArr: { [key: string]: any } = [uid];
        this.uploadService.getFiles(uid, 20).snapshotChanges().pipe(
          map(changes =>
            // store the key
            changes.map((c: { payload: { key: any; val: () => any; }; }) => ({ key: c.payload.key, ...c.payload.val() }))
          )
        ).subscribe((fileUploads: any) => {
          if (fileUploads.length != 0) {
            filesArr[uid] = fileUploads;
            this.userImgFilesLenght[uid] = fileUploads.length;
            if (this.allUsers?.[i].uid === filesArr[0]) {
              // console.log(filesArr[uid][0].url);
              this.allUsers![i].photoURL = filesArr[uid][0].url;
            }
          }
        });

        this.uploadService2.getFiles(uid, 10).snapshotChanges().pipe(
          map(changes =>
            // store the key
            changes.map((c: { payload: { key: any; val: () => any; }; }) => ({ key: c.payload.key, ...c.payload.val() }))
          )
        ).subscribe((fileUploads: any) => {
          if (fileUploads.length != 0) {
            this.userVidFilesLenght[uid] = fileUploads.length;
          }
        });
      }
    }
  }

  async getUserPrefs(): Promise<void> {
    if (this.allUsers != undefined) {
      for (let i = 0; i < this.allUsers?.length; i++) {
        const tempDatePrefs = this.allUsers[i].datePrefs;

        this.prefs[i] = [];

        for (const [key, value] of Object.entries(tempDatePrefs)) {
          if (value != false) {
            if (key == 'bd') {
              this.prefs[i].push('BDSM');
            }
            if (key == 'em') {
              this.prefs[i].push('Erotische massage');
            }
            if (key == 'es') {
              this.prefs[i].push('Escort');
            }
            if (key == 'gb') {
              this.prefs[i].push('Gangbang');
            }
            if (key == 'po') {
              this.prefs[i].push('Prive ontvangst');
            }
            if (key == 'rp') {
              this.prefs[i].push('Raamprostitutie');
            }
          }
        }
      }
    }
  }

  search(value: string): void {
    this.allUsers = this.origAllUsers?.filter((val) => val.displayName.toLowerCase().includes(value));
  }

  buttonClick(): void {
    this.status = !this.status;

    if (this.status) {
      this.toggleClass = "open"
    } else {
      this.toggleClass = "closed"
    }
  }

  handlePageChange(event: any) {
    this.page = event;
    sessionStorage.setItem('Activepage', JSON.stringify(this.page));
  }

  onChangeGender($event: any, gender: any) {
    const checked = $event.checked;
    let u: any = {};

    if (checked) {
      this.genderFilter = [...this.genderFilter, { ...gender }];
    } else {
      this.genderFilter = [...this.genderFilter.filter(x => x.id !== gender.id)];
    }

    for (let i = 0; i < this.genderFilter.length; i++) {
      u[this.genderFilter[i].name] = true;
    }

    sessionStorage.setItem('genderFiltered', JSON.stringify(u));
    sessionStorage.setItem('genderFilter', JSON.stringify(this.genderFilter));
  }

  onChangeHair($event: any, hairColor: any) {
    const checked = $event.checked;
    let u: any = {};

    if (checked) {
      this.hairFilter = [...this.hairFilter, { ...hairColor }];
    } else {
      this.hairFilter = [...this.hairFilter.filter(x => x.id !== hairColor.id)];
    }

    for (let i = 0; i < this.hairFilter.length; i++) {
      u[this.hairFilter[i].name] = true;
    }

    sessionStorage.setItem('hairFiltered', JSON.stringify(u));
    sessionStorage.setItem('hairFilter', JSON.stringify(this.hairFilter));
  }



  onChangeBody($event: any, bodyType: any) {
    const checked = $event.checked;
    let u: any = {};

    if (checked) {
      this.bodyFilter = [...this.bodyFilter, { ...bodyType }];
    } else {
      this.bodyFilter = [...this.bodyFilter.filter(x => x.id !== bodyType.id)];
    }

    for (let i = 0; i < this.bodyFilter.length; i++) {
      u[this.bodyFilter[i].name] = true;
    }

    sessionStorage.setItem('bodyFiltered', JSON.stringify(u));
    sessionStorage.setItem('bodyFilter', JSON.stringify(this.bodyFilter));
  }

  onChangeLanguage($event: any, languages: any) {
    const checked = $event.checked;
    let u: any = {};

    if (checked) {
      this.languageFilter = [...this.languageFilter, { ...languages }];
    } else {
      this.languageFilter = [...this.languageFilter.filter((x: { id: any; }) => x.id !== languages.id)];
    }

    for (let i = 0; i < this.languageFilter.length; i++) {
      u[this.languageFilter[i].name] = true;
    }

    sessionStorage.setItem('languageFiltered', JSON.stringify(u));
    sessionStorage.setItem('languageFilter', JSON.stringify(this.languageFilter));
  }

  onChangeOrigin($event: any, origin: any) {
    const checked = $event.checked;
    let u: any = {};

    if (checked) {
      this.originFilter = [...this.originFilter, { ...origin }];
    } else {
      this.originFilter = [...this.originFilter.filter(x => x.id !== origin.id)];
    }

    for (let i = 0; i < this.originFilter.length; i++) {
      u[this.originFilter[i].name] = true;
    }

    sessionStorage.setItem('originFiltered', JSON.stringify(u));
    sessionStorage.setItem('originFilter', JSON.stringify(this.originFilter));
  }

  onChangeServices($event: any, services: any) {
    const checked = $event.checked;
    let u: any = {};

    if (checked) {
      this.servicesFilter = [...this.servicesFilter, { ...services }];
    } else {
      this.servicesFilter = [...this.servicesFilter.filter((x: { id: any; }) => x.id !== services.id)];
    }

    for (let i = 0; i < this.servicesFilter.length; i++) {
      u[this.servicesFilter[i].code] = true;
    }

    sessionStorage.setItem('servicesFiltered', JSON.stringify(u));
    sessionStorage.setItem('servicesFilter', JSON.stringify(this.servicesFilter));
  }

  onChangeAge($event: any) {
    const changed = $event.value;
    if (changed) {
      this.ageFilter = changed;
    }
    sessionStorage.setItem('ageFilter', JSON.stringify(this.ageFilter));
  }

  getSliderTickInterval(): number | 'auto' {
    if (this.showTicks) {
      return this.autoTicks ? 'auto' : this.tickInterval;
    }
    return 0;
  }

  changeGrid() {
    this.gridView = !this.gridView;

    if (this.gridView) {
      this.showView = 'list';

    } else {
      this.showView = 'grid';
    }
    this.actionService.sendClass(this.showView);
  }

  toggleSearch(): void {
    this.searchView = !this.searchView;

    if (this.searchView) {
      this.showSearch = 'open';

    } else {
      this.showSearch = 'closed';
    }
  }

  clearSearch(): void {
    this.message = '';
    this.actionService.changeMessage('');
  }

  searchUserName(event: any): void {
    this.actionService.changeMessage(event.target.value);
  }
}

export function wait(nm: number) {
  return new Promise((res) => {
    setTimeout(res, nm);
  });
}