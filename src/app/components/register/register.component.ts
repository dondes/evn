import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { AuthService } from "../../shared/services/auth.service";

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.less']
})

export class RegisterComponent implements OnInit {
  hide = true;
  
  accountType: any = [
    {
      name: 'Advertentie',
      code: 'add',
      id: 1
    },
    {
      name: 'bezoeker',
      code: 'vis',
      id: 2
    },
    {
      name: 'Bedrijf',
      code: 'com',
      id: 3
    }
  ];

  constructor(
    private _location: Location,
    public authService: AuthService
  ) { }

  ngOnInit(): void {
  }

  backClicked() {
    this._location.back();
  }

}
