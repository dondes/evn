import { Component, OnInit } from '@angular/core';
import { FileUploadService } from '../../shared/services/file-upload.service';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-upload-list',
  templateUrl: './upload-list.component.html',
  styleUrls: ['./upload-list.component.less']
})
export class UploadListComponent implements OnInit {
  fileUploads?: any[];
  user_data: any;

  constructor(private uploadService: FileUploadService) { }

  ngOnInit(): void {
    this.user_data = JSON.parse(sessionStorage.user);

    this.uploadService.getFiles(this.user_data?.uid, 20).snapshotChanges().pipe(
      map(changes =>
        // store the key
        changes.map(c => ({ key: c.payload.key, ...c.payload.val() }))
      )
    ).subscribe(fileUploads => {
      this.fileUploads = fileUploads;
    });
  }

}
