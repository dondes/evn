import { Component, OnInit } from '@angular/core';
import { ActionService } from '../../shared/services/action.service';

@Component({
  selector: 'app-terms-and-conditions',
  templateUrl: './terms-and-conditions.component.html',
  styleUrls: ['./terms-and-conditions.component.less']
})
export class TermsAndConditionsComponent implements OnInit {

  constructor(private as : ActionService) { }

  ngOnInit(): void {
  }

  goToConsent(event:any, consent: string): void {
    this.as.openConsentDialog(event, consent);
  }

}
