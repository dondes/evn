import { Component, Inject, OnInit } from '@angular/core';
import { UserDataService } from '../../shared/services/user-data.service';
import { FileUploadService } from '../../shared/services/file-upload.service';
import { FileUpload2Service } from 'src/app/shared/services/file-upload2.service';
import { DOCUMENT, Location } from '@angular/common';
import { map } from 'rxjs/operators';
import { Editor, Toolbar } from 'ngx-editor';
import { MatDialog } from '@angular/material/dialog';
import { EmpolyeeFormComponent } from '../empolyee-form/empolyee-form.component';
import { EmployeeDataFormComponent } from '../employee-data-form/employee-data-form.component';

@Component({
  selector: 'app-company-form',
  templateUrl: './company-form.component.html',
  styleUrls: ['./company-form.component.less']
})
export class CompanyFormComponent implements OnInit {

  editor: Editor = new Editor;

  toolbar: Toolbar = [
    ["bold", "italic"],
    ["underline", "strike"],
    ["ordered_list", "bullet_list"],
  ];

  allComplete: boolean = false;
  videoUploads?: any[];
  fileUploads?: any[];
  firstFileUrl?: string;

  infoText = '';

  childUid = '';

  // provincies en gemeentes
  prov_gems = this.userDataService.prov_gems;
  // Languages
  languages = this.userDataService.languages;
  // Date preferences
  date_preference = this.userDataService.date_preference;
  // Services
  dateServices = this.userDataService.dateServices;
  // Prices
  prices_po = this.userDataService.prices_po;
  // UserData array
  user_data: any;

  prices_es = this.userDataService.prices_es;

  // localData: any;

  curUserData: any;

  childAccs: any[]= [];

  siteImg = "../../../assets/profile.svg";

  // Availability
  availability = this.userDataService.availability;

  constructor(
    private uploadService: FileUploadService,
    private uploadService2: FileUpload2Service,
    public userDataService: UserDataService,
    @Inject(DOCUMENT) public _document: Document,
    public dialog: MatDialog,
  ) { }

  async ngOnInit(): Promise<void> {
    this.dateServices = this.dateServices.sort((a: { name: string; }, b: { name: string; }) => a.name !== b.name ? a.name < b.name ? -1 : 1 : 0);

    await this.getUserData();

    // this.localData = JSON.parse(sessionStorage.user);

    

    // this.infoText = this.user_data?.info;

    
  }

  async getUserData() {
    let userDData = JSON.parse(sessionStorage.user);

    if (sessionStorage.getItem('user')!.length === 9) {
      this._document.defaultView?.location.reload();
    }
    
    this.userDataService.getUser(userDData.uid).valueChanges().subscribe(val => {
      this.user_data = val;
      this.infoText = this.user_data.info;

      this.getChildAccounts();
    });
    

    this.uploadService.getFiles(userDData.uid, 20).snapshotChanges().pipe(
      map(changes =>
        // store the key
        changes.map(c => ({ key: c.payload.key, ...c.payload.val() }))
      )
    ).subscribe(fileUploads => {
      this.fileUploads = fileUploads;
      
      this.firstFileUrl = (this.fileUploads.length !== 0 ? this.fileUploads[0].url : '../../../assets/profile.svg');
    });

    this.uploadService2.getFiles(userDData.uid, 20).snapshotChanges().pipe(
      map(changes =>
        // store the key
        changes.map(c => ({ key: c.payload.key, ...c.payload.val() }))
      )
    ).subscribe(fileUploads => {
      this.videoUploads = fileUploads;
    });
  }

  async getChildAccounts() {
    for (let i = 0; i < this.user_data?.parentOf.length; i++) {
      let u;
      this.userDataService.getUser(this.user_data.parentOf[i]).valueChanges().subscribe(val => {
        this.childAccs[i] = val;
        
        this.uploadService.getFiles(this.childAccs[i]?.uid, 20).snapshotChanges().pipe(
          map(changes =>
            // store the key
            changes.map(c => ({ key: c.payload.key, ...c.payload.val() }))
          )
        ).subscribe(fileUploads => {
          u = fileUploads;
          this.childAccs[i].photoURL = u[0]?.url;
        });
      });
    }
  }

  async onSubmit() {

    this.curUserData = JSON.parse(sessionStorage.user);
    // this.userDataService.form.value.user_data = this.user_data;
    let data = this.userDataService.form.value;

    data.childNum = this.user_data.childNum;

    data.parentOf = this.user_data.parentOf;

    if (data.Maandag.hd === false) {
      if ((data.Maandag.bt.length == 0) && (data.Maandag.et.length == 0)) {
        data.Maandag.nb = true;
        data.Maandag.bt = '';
        data.Maandag.et = '';
      }
    } else {
      data.Maandag.nb = false;
      data.Maandag.bt = '';
      data.Maandag.et = '';
    }

    if (data.Dinsdag.hd === false) {
      if ((data.Dinsdag.bt.length == 0) && (data.Dinsdag.et.length == 0)) {
        data.Dinsdag.nb = true;
        data.Dinsdag.bt = '';
        data.Dinsdag.et = '';
        data.Dinsdag.hd = false;
      }
    } else {
      data.Dinsdag.nb = false;
      data.Dinsdag.bt = '';
      data.Dinsdag.et = '';
    }

    if (data.Woensdag.hd === false) {
      if ((data.Woensdag.bt.length == 0) && (data.Woensdag.et.length == 0)) {
        data.Woensdag.nb = true;
        data.Woensdag.bt = '';
        data.Woensdag.et = '';
        data.Woensdag.hd = false;
      }
    } else {
      data.Woensdag.nb = false;
      data.Woensdag.bt = '';
      data.Woensdag.et = '';
    }

    if (data.Donderdag.hd === false) {
      if ((data.Donderdag.bt.length == 0) && (data.Donderdag.et.length == 0)) {
        data.Donderdag.nb = true;
        data.Donderdag.bt = '';
        data.Donderdag.et = '';
        data.Donderdag.hd = false;
      }
    } else {
      data.Donderdag.nb = false;
      data.Donderdag.bt = '';
      data.Donderdag.et = '';
    }

    if (data.Vrijdag.hd === false) {
      if ((data.Vrijdag.bt.length == 0) && (data.Vrijdag.et.length == 0)) {
        data.Vrijdag.nb = true;
        data.Vrijdag.bt = '';
        data.Vrijdag.et = '';
        data.Vrijdag.hd = false;
      }
    } else {
      data.Vrijdag.nb = false;
      data.Vrijdag.bt = '';
      data.Vrijdag.et = '';
    }

    if (data.Zaterdag.hd === false) {
      if ((data.Zaterdag.bt.length == 0) && (data.Zaterdag.et.length == 0)) {
        data.Zaterdag.nb = true;
        data.Zaterdag.bt = '';
        data.Zaterdag.et = '';
        data.Zaterdag.hd = false;
      }
    } else {
      data.Zaterdag.nb = false;
      data.Zaterdag.bt = '';
      data.Zaterdag.et = '';
    }

    if (data.Zondag.hd === false) {
      if ((data.Zondag.bt.length == 0) && (data.Zondag.et.length == 0)) {
        data.Zondag.nb = true;
        data.Zondag.bt = '';
        data.Zondag.et = '';
        data.Zondag.hd = false;
      }
    } else {
      data.Zondag.nb = false;
      data.Zondag.bt = '';
      data.Zondag.et = '';
    }

    if (data.displayName.length === 0) {
      data.displayName = (this.user_data.displayName.length == 0) ? '' : this.user_data.displayName;
    } else {
      this.curUserData.displayName = data.displayName;
    }

    if (data.provGem.gem.length === 0) {
      data.provGem = this.user_data.provGem;
    } else {

      // split gem form data
      var splitted = data.prov_gem.gem.split(";", 2);
      // change form prov_data data to splitted data
      data.prov_gem.prov = splitted[1];
      data.prov_gem.gem = splitted[0];

      // add form data to curUserData
      this.curUserData.provGem = data.provGem;
    }

    if (data.introTitle.length === 0) {
      data.introTitle = this.user_data.introTitle;
    } else {
      this.curUserData.introTitle = data.introTitle;
    }

    if (this.infoText.length === 0) {
      data.info = this.user_data.info;
    } else {
      this.curUserData.info = this.infoText;
    }

    if (data.mobNumber.length === 0) {
      data.mobNumber = this.user_data.mobNumber;
    } else {
      this.curUserData.mobNumber = data.mobNumber;
    }

    if (data.phoneNumber.length === 0) {
      data.phoneNumber = this.user_data.phoneNumber;
    } else {
      this.curUserData.phoneNumber = data.phoneNumber;
    }

    if (data.street.length === 0) {
      data.street = this.user_data.street;
    } else {
      this.curUserData.street = data.street;
    }

    if (data.addative.length === 0) {
      data.addative = this.user_data.addative.toUpperCase();
    } else {
      this.curUserData.addative = data.addative.toUpperCase();
    }

    if (data.zipcode.length === 0) {
      data.zipcode = this.user_data.zipcode.toUpperCase();
    } else {
      this.curUserData.zipcode = data.zipcode.toUpperCase();
    }

    if (data.houseNr.length === 0) {
      data.houseNr = this.user_data.houseNr;
    } else {
      this.curUserData.houseNr = data.houseNr;
    }


    sessionStorage.clear();
    sessionStorage.setItem('user', JSON.stringify(this.curUserData));
    
    await this.userDataService.updateUserData(data, this.curUserData.uid, Date.now());
    // this.userDataService.createUserData(data).then(res =>{
    //       /*do something here....
    //          maybe clear the form or give a success message*/
    // });



    // this.refreshPage();
  }

  refreshPage() {
    this.curUserData = JSON.parse(sessionStorage.user);
    this._document.defaultView?.location.reload();
  }

  openDialog(formType:any): void {
    this.dialog.open(formType);
  }

  onClick(event: { target: any; }) {
    // var target = event?.target;
    this.openDialog(EmpolyeeFormComponent);
  }

  onClick1(event: { target: any; }, uid:string) {
    this.userDataService.childUid = uid;
    // var target = event?.target;
    this.openDialog(EmployeeDataFormComponent);
  }

}

