import { Component, OnInit } from '@angular/core';
import {Location} from '@angular/common';
import { AuthService } from "../../shared/services/auth.service";
import { UserDataService } from "../../shared/services/user-data.service";
import { FileUploadService as UploadService } from "../../shared/services/file-upload.service";
import { FileUpload2Service as UploadService2 } from '../../shared/services/file-upload2.service';
import { Router } from "@angular/router";

@Component({
  selector: 'app-deregister',
  templateUrl: './deregister.component.html',
  styleUrls: ['./deregister.component.less']
})

export class DeRegisterComponent implements OnInit {
  hide = true;
  // User: any = ['Super Admin', 'Author', 'Reader'];

  constructor(
    private _location: Location,
    public authService: AuthService,
    public userDataService: UserDataService,
    public uploadService: UploadService,
    public uploadService2: UploadService2,    
    public router: Router
  ) { }

  ngOnInit(): void {
  }

  backClicked() {
    this._location.back();
  }

  async onRemoveRegistry(userEmail: string|any , userPwd:string) {

    const userid = await this.authService.GetAuthUser(userEmail, userPwd);

    if (userid) {
      this.userDataService.deleteUserData(userid);       
      this.uploadService.deleteStorage(userid);
      this.uploadService2.deleteStorage(userid);          
    }
    await this.authService.RemoveRegistry(userEmail, userPwd);
    this.authService.SignOut(); 
  }


}
