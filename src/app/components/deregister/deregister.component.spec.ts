import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DeRegisterComponent } from './deregister.component';

describe('DeRegisterComponent', () => {
  let component: DeRegisterComponent;
  let fixture: ComponentFixture<DeRegisterComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DeRegisterComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DeRegisterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
