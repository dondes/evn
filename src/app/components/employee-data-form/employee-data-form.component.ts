import { Component, Inject, Input, OnInit } from '@angular/core';
import { UserDataService } from '../../shared/services/user-data.service';
import { FileUploadService } from '../../shared/services/file-upload.service';
import { FileUpload2Service } from 'src/app/shared/services/file-upload2.service';
import { DOCUMENT, Location } from '@angular/common';
import { map } from 'rxjs/operators';
import { Editor, Toolbar } from 'ngx-editor';
import { MatDialogRef } from '@angular/material/dialog';
import { FileUpload } from '../../shared/models/file-upload.model';


@Component({
  selector: 'app-employee-data-form',
  templateUrl: './employee-data-form.component.html',
  styleUrls: ['./employee-data-form.component.less']
})

export class EmployeeDataFormComponent implements OnInit {
  @Input() fileUpload!: FileUpload;
  uid = '';
  
  editor: Editor = new Editor ;

  toolbar: Toolbar = [
    ["bold", "italic"],
    ["underline", "strike"],
    ["ordered_list", "bullet_list"],
  ];

  aFiles: number = 0;
  selectedFiles?: FileList;
  currentFileUpload?: FileUpload;
  percentage = 0;

  allComplete: boolean = false;
  videoUploads?: any[];
  fileUploads?: any[];
  firstFileUrl?: string;

  infoText = '';

  siteImg = "../../../assets/profile.svg";

  // provincies en gemeentes
  prov_gems = this.userDataService.prov_gems;

  // gender
  gender = this.userDataService.gender;

  // breast size
  breast_size = this.userDataService.breast_size;

  // origins
  origins = this.userDataService.origins;

  // Languages
  languages = this.userDataService.languages;

  // hair colors
  hair_color = this.userDataService.hair_color;

  // Body types
  body_type = this.userDataService.body_type;

  // Date preferences
  date_preference = this.userDataService.date_preference;

  // Sexual preferences
  sexualPref = this.userDataService.sexualPref;

  // Services
  dateServices = this.userDataService.dateServices;

  // Prices
  prices_po = this.userDataService.prices_po;
  
  prices_es = this.userDataService.prices_es;
  
  // Availability
  availability = this.userDataService.availability;
  
  // UserData array
  child_data: any;

  selected: string = '';

  genderContent: string = '';

  dateTypeContent: string = '';

  constructor(
    private location: Location, 
    private uploadService: FileUploadService, 
    private uploadService2: FileUpload2Service, 
    public userDataService: UserDataService,
    public dialogRef: MatDialogRef<EmployeeDataFormComponent>,
    @Inject(DOCUMENT) public _document: Document
  ) { }

  async ngOnInit(): Promise<void> {
    await this.getUserData();

    this.dateServices = this.dateServices.sort((a: { name: string; }, b: { name: string; }) => a.name !== b.name ? a.name < b.name ? -1 : 1 : 0);
    // console.log(this.uidFromParent);
    
    this.uploadService.getFiles(this.uid, 20).snapshotChanges().pipe(
      map(changes =>
        // store the key
        changes.map(c => ({ key: c.payload.key, ...c.payload.val() }))
      )
    ).subscribe(fileUploads => {
      this.fileUploads = fileUploads;
    
      this.firstFileUrl = (this.fileUploads.length !== 0 ? this.fileUploads[0].url : '../../../assets/profile.svg') ;
    });

    this.uploadService2.getFiles(this.child_data?.uid, 20).snapshotChanges().pipe(
      map(changes =>
        // store the key
        changes.map(c => ({ key: c.payload.key, ...c.payload.val() }))
      )
    ).subscribe(fileUploads => {
      this.videoUploads = fileUploads;
    });

    this.infoText = this.child_data?.info;
  }

  async onSubmit() {

    // this.userDataService.form.value.child_data = this.child_data;
    let data = this.userDataService.form.value;
    
    if(data.Maandag.hd === false ) {
      if((data.Maandag.bt.length == 0) && (data.Maandag.et.length == 0)){
        data.Maandag.nb = true;
        data.Maandag.bt = '';
        data.Maandag.et = '';
      }
    } else {
      data.Maandag.nb = false;
      data.Maandag.bt = '';
      data.Maandag.et = '';
    }

    if(data.Dinsdag.hd === false ) {
      if((data.Dinsdag.bt.length == 0) && (data.Dinsdag.et.length == 0)){
        data.Dinsdag.nb = true;
        data.Dinsdag.bt = '';
        data.Dinsdag.et = '';
        data.Dinsdag.hd = false;
      }
    } else {
      data.Dinsdag.nb = false;
      data.Dinsdag.bt = '';
      data.Dinsdag.et = '';
    }
    
    if(data.Woensdag.hd === false ) {
      if((data.Woensdag.bt.length == 0) && (data.Woensdag.et.length == 0)){
        data.Woensdag.nb = true;
        data.Woensdag.bt = '';
        data.Woensdag.et = '';
        data.Woensdag.hd = false;
      }
    } else {
      data.Woensdag.nb = false;
      data.Woensdag.bt = '';
      data.Woensdag.et = '';
    }
    
    if(data.Donderdag.hd === false ) {
      if((data.Donderdag.bt.length == 0) && (data.Donderdag.et.length == 0)){
        data.Donderdag.nb = true;
        data.Donderdag.bt = '';
        data.Donderdag.et = '';
        data.Donderdag.hd = false;
      }
    } else {
      data.Donderdag.nb = false;
      data.Donderdag.bt = '';
      data.Donderdag.et = '';
    }

    if(data.Vrijdag.hd === false ) {
      if((data.Vrijdag.bt.length == 0) && (data.Vrijdag.et.length == 0)){
        data.Vrijdag.nb = true;
        data.Vrijdag.bt = '';
        data.Vrijdag.et = '';
        data.Vrijdag.hd = false;
      }
    } else {
      data.Vrijdag.nb = false;
      data.Vrijdag.bt = '';
      data.Vrijdag.et = '';
    }

    if(data.Zaterdag.hd === false ) {
      if((data.Zaterdag.bt.length == 0) && (data.Zaterdag.et.length == 0)){
        data.Zaterdag.nb = true;
        data.Zaterdag.bt = '';
        data.Zaterdag.et = '';
        data.Zaterdag.hd = false;
      }
    } else {
      data.Zaterdag.nb = false;
      data.Zaterdag.bt = '';
      data.Zaterdag.et = '';
    }

    if(data.Zondag.hd === false ) {
      if((data.Zondag.bt.length == 0) && (data.Zondag.et.length == 0)){
        data.Zondag.nb = true;
        data.Zondag.bt = '';
        data.Zondag.et = '';
        data.Zondag.hd = false;
      }
    } else {
      data.Zondag.nb = false;
      data.Zondag.bt = '';
      data.Zondag.et = '';
    }
    
    if (data.displayName.length === 0) {
      data.displayName = (this.child_data.displayName.length == 0)? '' : this.child_data.displayName;
    }
    console.log(data);
    console.log(this.child_data);
    if (data.provGem.gem.length === 0) {
      data.prov_gem = this.child_data.prov_gem;
    } else {
      
      // split gem form data
      var splitted = data.prov_gem.gem.split(";",2);
      // change form prov_data data to splitted data
      data.prov_gem.prov = splitted[1];
      data.prov_gem.gem = splitted[0];
    }

    if (data.introTitle.length === 0) {
      data.introTitle = this.child_data.introTitle;
    }

    if (this.infoText.length === 0) {
      data.info = this.child_data.info;
    }

    if (data.age === 0) {
      data.age = this.child_data.age;
    }

    if (data.origin.length === 0) {
      data.origin = this.child_data.origin;
    }

    if (data.hairColor.length === 0) {
      data.hairColor = this.child_data.hairColor;
    }

    if (data.bodyType.length === 0) {
      data.bodyType = this.child_data.bodyType;
    }

    if (data.sexualPref.length === 0) {
      data.sexualPref = this.child_data.sexualPref;
    }

    if (data.lengte.length === 0) {
      data.lengte = this.child_data.lengte;
    }

    if (data.mobNumber.length === 0) {
      data.mobNumber = this.child_data.mobNumber;
    }

    if (data.gender.length === 0) {
      data.gender = this.child_data.gender;
      switch (data.gender) {
        case 'Female':
          data.penisSize = '';
          
          if (data.breastSize.length === 0) {
            data.breastSize = this.child_data.breastSize;
          }
          break;
        case 'Male':
          data.penisSize = (data.penisSize === null ? '' : data.penisSize);
          data.breastSize = '';
          
          if (data.penisSize.length === 0) {
            data.penisSize = this.child_data.penisSize;
          }
          break;
        case 'Shemale':
          if (data.breastSize.length === 0) {
            data.breastSize = this.child_data.breastSize;
          }
          if (data.penisSize.length === 0) {
            data.penisSize = this.child_data.penisSize;
          }
          break;
        case 'Couple':
          if (data.breastSize.length === 0) {
            data.breastSize = this.child_data.breastSize;
          }
          if (data.penisSize.length === 0) {
            data.penisSize = this.child_data.penisSize;
          }
          break;
      }
    } else {
      switch (data.gender) {
        case 'Female':
          data.penisSize = '';
          if (data.breastSize.length === 0) {
            data.breastSize = this.child_data.breastSize;
          }
          break;
        case 'Male':
          data.penisSize = (data.penisSize === null ? '' : data.penisSize);
          data.breastSize = '';
          
          if (data.penisSize.length === 0) {
            data.penisSize = this.child_data.penisSize;
          }
          break;
        case 'Shemale':
          data.penisSize = (data.penisSize === null ? '' : data.penisSize);
          data.breastSize = (data.breastSize === null ? '' : data.breastSize);
          
          if (data.breastSize.length === 0) {
            data.breastSize = this.child_data.breastSize;
          }
          if (data.penisSize.length === 0) {
            data.penisSize = this.child_data.penisSize;
          }
          break;
        case 'Couple':
          data.penisSize = (data.penisSize === null ? '' : data.penisSize);
          data.breastSize = (data.breastSize === null ? '' : data.breastSize);
          
          if (data.breastSize.length === 0) {
            data.breastSize = this.child_data.breastSize;
          }
          if (data.penisSize.length === 0) {
            data.penisSize = this.child_data.penisSize;
          }
          break;
      }
    }

    await this.userDataService.updateUserData(data, this.child_data?.uid, Date.now());
    // this.userDataService.createUserData(data).then(res =>{
    //       /*do something here....
    //          maybe clear the form or give a success message*/
    // });



    // this.refreshPage();
  }

  radioButtonChanged($event: { source: { name: any; }; value: string; }) {
    // if you need the event source and value it can be accessed.
    // console.log($event.source.name, '---'+$event.value);

    switch ($event.value) {
      case 'Female':
        this.genderContent = $event.value;
        this.userDataService.clearInputMethod();
        break;
      case 'Male':
        this.genderContent = $event.value;
        this.userDataService.clearInputMethod();
        break;
      case 'Shemale':
        this.genderContent = $event.value;
        this.userDataService.clearInputMethod();
        break;
      case 'Couple':
        this.genderContent = $event.value;
        this.userDataService.clearInputMethod();
        break;
    }
  }

  checkButtonChanged($event: { source: { name: any; }; value: string; }) {
    switch ($event.value) {
      case 'es':
        this.dateTypeContent = $event.value;

        break;
      case 'po':
        this.dateTypeContent = $event.value;
        break;
    }
  }

  async getUserData() {
    this.uid = this.userDataService.childUid;
    this.userDataService.getUser(this.uid).valueChanges().subscribe(val => {
      this.child_data = val;
      this.infoText = this.child_data.info;
      this.genderContent = this.child_data.gender;
    });
    
  }

  refreshPage() {
    this._document.defaultView?.location.reload();
  }

  close() {
    this.dialogRef.close();
  }

  selectFile(event: any): void {
    this.selectedFiles = event.target.files;
    this.aFiles = this.selectedFiles!.length;
  }

  upload(): void {
    for (let i = 0; i <= this.selectedFiles!.length ; i++) {
      const file: File | null = this.selectedFiles!.item(i);
      // this.selectedFiles = undefined;

      if (file) {
        this.currentFileUpload = new FileUpload(file);
        this.uploadService.pushFileToStorage(this.child_data.uid, this.currentFileUpload).subscribe(
          percentage => {
            this.percentage = Math.round(percentage ? percentage : 0);
          },
          error => {
            console.log(error);
          }
        );
      }
    }

    this.selectedFiles = undefined;
  }

  deleteFileUpload(fileUpload: FileUpload): void {
    this.uploadService.deleteFile(this.child_data?.uid, fileUpload);
  }
}