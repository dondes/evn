import { Component, EventEmitter, Input, OnInit, Output, ViewChild  } from '@angular/core';
import { UserDataService } from 'src/app/shared/services/user-data.service';
import { map } from "rxjs/operators";
import { FileUploadService } from '../../shared/services/file-upload.service';
import { FileUpload2Service } from '../../shared/services/file-upload2.service';
import { MatDialog, MatDialogConfig, MatDialogRef } from '@angular/material/dialog';
import { DialogBodyComponent } from '../dialog-body/dialog-body.component';
import { DomSanitizer } from '@angular/platform-browser';
import { Editor, Toolbar } from 'ngx-editor';

@Component({
  selector: 'app-employee-details',
  templateUrl: './employee-details.component.html',
  styleUrls: ['./employee-details.component.less'],
})
export class EmployeeDetailsComponent implements OnInit {

  @Input() rating: number = 0;
  @Input() itemId: string = '';
  @Output() ratingClick: EventEmitter<any> = new EventEmitter<any>();
  
  editor: Editor = new Editor;

  toolbar: Toolbar = [
    ["bold", "italic"],
    ["underline", "strike"],
    ["ordered_list", "bullet_list"],
  ];

  html: '' = "";

  inputName: string = '';
  
  siteImg = "../../../assets/profile.svg";

  public slide = "slideId-0";

  uid = this.userDataService.childUid;

  user: any;

  userAuth: any;

  userReviews: any[] = [];

  userRatings: any[] = [];

  userReviewers: any[] = [];

  scorePercentage: any[] = [];

  average: any;

  selectedIndex!: number;

  averagePercentage: any;

  userImgFiles: any[] = [];

  userVidFiles: any[] = [];

  languages: string[] = [];

  prefs: string[] = [];

  servs: any[] = [];

  imgArr: any[] = [];

  vidArr?: FileList;

  idAttr: any;

  thumbnailData: any;

  // po vars
  po: boolean = false;
  qy: any;
  hu: any;
  dk: any;
  eu: any;
  tu: any;
  vu: any;

  // es vars
  es: boolean = false;
  es_eu: any;
  es_tu: any;
  es_vu: any;
  es_au: any;
  es_hd: any;
  es_wd: any;

  week: any[] = [];

  flipped: boolean = false;

  status: boolean = false;

  statusReview: boolean = false;

  blockReview: boolean = false;

  tab_status: any;

  hygene: any;

  location: any;

  hygenePercentage: any;

  hygeneAverage: any;

  locationPercentage: any;

  locationAverage: any;

  showStatus = "Meer tonen";

  btnVal = "Toon nummer";

  constructor(
    private userDataService: UserDataService,
    private uploadImgService: FileUploadService,
    private uploadVidService: FileUpload2Service,
    public dialog: MatDialog,
    public dialogRef: MatDialogRef<EmployeeDetailsComponent>,
    private sanitizer: DomSanitizer
  ) { }

  ngOnDestroy(): void {
    this.editor?.destroy();
    // throw new Error('Method not implemented.');
  }

  async ngOnInit(): Promise<void> {

    await this.getUserData();
    await this.getUserReviews();

    for (let i = 0; i < this.userReviews.length; i++) {
      this.userReviews[i].revs = this.userReviews[i].revs.sort((a: { name: string; }, b: { name: string; }) => a.name.localeCompare(b.name));
    }

    this.inputName = this.itemId + '_rating';

    this.editor = new Editor();
  }

  async getUserData() {
    this.userDataService.getUser(this.uid).valueChanges().subscribe(val => {
      this.user = val;

      this.user.info = this.sanitizer.bypassSecurityTrustHtml(this.user.info);

      this.getUserImgFiles(this.uid);
      this.getUserVidFiles(this.uid);
      
      const tempLang = this.user.languages;
      for (const [key, value] of Object.entries(tempLang)) {
        if (value != false) {
          if (key == 'ned') {
            this.languages?.push('Nederlands');
          }
          if (key == 'dui') {
            this.languages?.push('Duits');
          }
          if (key == 'spa') {
            this.languages?.push('Spaans');
          }
          if (key == 'fra') {
            this.languages?.push('Frans');
          }
          if (key == 'eng') {
            this.languages?.push('Engels');
          }
        }
      }

      const tempDatePrefs = this.user.datePrefs;
      for (const [key, value] of Object.entries(tempDatePrefs)) {
        if (value != false) {
          if (key == 'bd') {
            this.prefs?.push('BDSM');
          }
          if (key == 'em') {
            this.prefs?.push('Erotische massage');
          }
          if (key == 'es') {
            this.prefs?.push('Escort');
            this.es = true;
          }
          if (key == 'gb') {
            this.prefs?.push('Gangbang');
          }
          if (key == 'po') {
            this.prefs?.push('Prive ontvangst');
            this.po = true;
          }
          if (key == 'rp') {
            this.prefs?.push('Raamprostitutie');
          }
        }
      }

      const tempDateServs = this.user.dateServices;
      console.log(tempDateServs);
      for (const [key, value] of Object.entries(tempDateServs)) {
        if (value != false) {
          if (key == 'abk') {
            this.servs?.push({ 'id': key, 'name': 'Anaal bij klant', 'rating': 0 });
          }
          if (key == 'bis') {
            this.servs?.push({ 'id': key, 'name': 'Bi seks', 'rating': 0 });
          }
          if (key == 'bus') {
            this.servs?.push({ 'id': key, 'name': 'Buiten seks', 'rating': 0 });
          }
          if (key == 'gan') {
            this.servs?.push({ 'id': key, 'name': 'Gangbang', 'rating': 0 });
          }
          if (key == 'kim') {
            this.servs?.push({ 'id': key, 'name': 'Klaarkomen in mond', 'rating': 0 });
          }
          if (key == 'kog') {
            this.servs?.push({ 'id': key, 'name': 'Klaarkomen op gezicht', 'rating': 0 });
          }
          if (key == 'pdk') {
            this.servs?.push({ 'id': key, 'name': 'Plassex door klant', 'rating': 0 });
          }
          if (key == 'pzc') {
            this.servs?.push({ 'id': key, 'name': 'Pijpen zonder condoom', 'rating': 0 });
          }
          if (key == 'tbr') {
            this.servs?.push({ 'id': key, 'name': 'Tussen borsten (russisch)', 'rating': 0 });
          }
          if (key == 'bko') {
            this.servs?.push({ 'id': key, 'name': 'BDSM klant onderdanig', 'rating': 0 });
          }
          if (key == 'tmm') {
            this.servs?.push({ 'id': key, 'name': 'Trio m/m', 'rating': 0 });
          }
          if (key == 'tmv') {
            this.servs?.push({ 'id': key, 'name': 'Trio m/v', 'rating': 0 });
          }
          if (key == 'zoe') {
            this.servs?.push({ 'id': key, 'name': 'Zoenen', 'rating': 0 });
          }
          if (key == 'mmh') {
            this.servs?.push({ 'id': key, 'name': 'Massage met hoogtepunt', 'rating': 0 });
          }
          if (key == 'bef') {
            this.servs?.push({ 'id': key, 'name': 'Beffen', 'rating': 0 });
          }
          if (key == 'int') {
            this.servs?.push({ 'id': key, 'name': 'Intiem', 'rating': 0 });
          }
          if (key == 'kbk') {
            this.servs?.push({ 'id': key, 'name': 'Kontlikken bij klant', 'rating': 0 });
          }
          if (key == 'szc') {
            this.servs?.push({ 'id': key, 'name': 'Sex zonder condoom', 'rating': 0 });
          }
          if (key == 'bkd') {
            this.servs?.push({ 'id': key, 'name': 'BDSM klant dominant', 'rating': 0 });
          }
          if (key == 'hic') {
            this.servs?.push({ 'id': key, 'name': 'Highclass companion', 'rating': 0 });
          }
          if (key == 'ssm') {
            this.servs?.push({ 'id': key, 'name': 'Soft SM', 'rating': 0 });
          }
          if (key == 'pij') {
            this.servs?.push({ 'id': key, 'name': 'Pijpen', 'rating': 0 });
          }
          if (key == 'spk') {
            this.servs?.push({ 'id': key, 'name': 'Speciale kledingverzoeken', 'rating': 0 });
          }
          if (key == 'ton') {
            this.servs?.push({ 'id': key, 'name': 'Tongzoenen', 'rating': 0 });
          }
          if (key == 'pla') {
            this.servs?.push({ 'id': key, 'name': 'Plassex', 'rating': 0 });
          }
          if (key == 'prm') {
            this.servs?.push({ 'id': key, 'name': 'Prostaat massage', 'rating': 0 });
          }
          if (key == 'vin') {
            this.servs?.push({ 'id': key, 'name': 'Vingeren', 'rating': 0 });
          }
          if (key == 'ana') {
            this.servs?.push({ 'id': key, 'name': 'Anaal', 'rating': 0 });
          }
          if (key == 'pdt') {
            this.servs?.push({ 'id': key, 'name': 'Pijpen deepthroat', 'rating': 0 });
          }
          if (key == 'squ') {
            this.servs?.push({ 'id': key, 'name': 'Squirting', 'rating': 0 });
          }
          if (key == 'fis') {
            this.servs?.push({ 'id': key, 'name': 'Fisting', 'rating': 0 });
          }
          if (key == 'fbk') {
            this.servs?.push({ 'id': key, 'name': 'Fisting bij klant', 'rating': 0 });
          }
          if (key == 'aft') {
            this.servs?.push({ 'id': key, 'name': 'Aftrekken', 'rating': 0 });
          }
          if (key == 'pem') {
            this.servs?.push({ 'id': key, 'name': 'Penismassage', 'rating': 0 });
          }
          if (key == 'asw') {
            this.servs?.push({ 'id': key, 'name': 'Ass worship', 'rating': 0 });
          }
          if (key == 'bon') {
            this.servs?.push({ 'id': key, 'name': 'Bondage', 'rating': 0 });
          }
          if (key == 'bhh') {
            this.servs?.push({ 'id': key, 'name': 'Boot / High heel worship', 'rating': 0 });
          }
          if (key == 'brp') {
            this.servs?.push({ 'id': key, 'name': 'Breath play', 'rating': 0 });
          }
          if (key == 'can') {
            this.servs?.push({ 'id': key, 'name': 'Caning', 'rating': 0 });
          }
          if (key == 'cab') {
            this.servs?.push({ 'id': key, 'name': 'Cock and Balls', 'rating': 0 });
          }
          if (key == 'evw') {
            this.servs?.push({ 'id': key, 'name': 'Electro / Violet wand', 'rating': 0 });
          }
          if (key == 'gag') {
            this.servs?.push({ 'id': key, 'name': 'Gagging / Gags', 'rating': 0 });
          }
          if (key == 'gos') {
            this.servs?.push({ 'id': key, 'name': 'Golden shower', 'rating': 0 });
          }
          if (key == 'kug') {
            this.servs?.push({ 'id': key, 'name': 'Kuisheidsgordel', 'rating': 0 });
          }
          if (key == 'lar') {
            this.servs?.push({ 'id': key, 'name': 'Lak / Rubber', 'rating': 0 });
          }
          if (key == 'mas') {
            this.servs?.push({ 'id': key, 'name': 'Masks', 'rating': 0 });
          }
          if (key == 'mum') {
            this.servs?.push({ 'id': key, 'name': 'Mummificatie', 'rating': 0 });
          }
          if (key == 'naa') {
            this.servs?.push({ 'id': key, 'name': 'Naalden', 'rating': 0 });
          }
          if (key == 'nit') {
            this.servs?.push({ 'id': key, 'name': 'Nipple torture', 'rating': 0 });
          }
          if (key == 'nur') {
            this.servs?.push({ 'id': key, 'name': 'Nursing', 'rating': 0 });
          }
          if (key == 'ops') {
            this.servs?.push({ 'id': key, 'name': 'Opsluiting', 'rating': 0 });
          }
          if (key == 'put') {
            this.servs?.push({ 'id': key, 'name': 'Publieke tentoonstelling', 'rating': 0 });
          }
          if (key == 'sou') {
            this.servs?.push({ 'id': key, 'name': 'Sounding', 'rating': 0 });
          }
          if (key == 'spi') {
            this.servs?.push({ 'id': key, 'name': 'Spitting', 'rating': 0 });
          }
          if (key == 'tra') {
            this.servs?.push({ 'id': key, 'name': 'Travestie', 'rating': 0 });
          }
          if (key == 'veh') {
            this.servs?.push({ 'id': key, 'name': 'Verbal humiliation', 'rating': 0 });
          }
          if (key == 'ver') {
            this.servs?.push({ 'id': key, 'name': 'Vernedering', 'rating': 0 });
          }
          if (key == 'wor') {
            this.servs?.push({ 'id': key, 'name': 'Worstelen', 'rating': 0 });
          }
          if (key == 'vos') {
            this.servs?.push({ 'id': key, 'name': 'Voorbinddildo sex', 'rating': 0 });
          }
          if (key == 'vio') {
            this.servs?.push({ 'id': key, 'name': 'Video opname', 'rating': 0 });
          }
        }

        this.servs = this.servs!.sort((a, b) => a.name.localeCompare(b.name));
      }
      // console.log(this.servs);

      const tempPoPrices = this.user?.poPrices;
      for (const [key, value] of Object.entries(tempPoPrices)) {
        if (key == 'qy') {
          this.qy = value;
        }
        if (key == 'hu') {
          this.hu = value;
        }
        if (key == 'dk') {
          this.dk = value;
        }
        if (key == 'eu') {
          this.eu = value;
        }
        if (key == 'tu') {
          this.tu = value;
        }
        if (key == 'vu') {
          this.vu = value;
        }
      }

      const tempEsPrices = this.user?.esPrices
      for (const [key, value] of Object.entries(tempEsPrices)) {
        if (key == 'eu') {
          this.es_eu = value;
        }
        if (key == 'tu') {
          this.es_tu = value;
        }
        if (key == 'vu') {
          this.es_vu = value;
        }
        if (key == 'au') {
          this.es_au = value;
        }
        if (key == 'hd') {
          this.es_hd = value;
        }
        if (key == 'wd') {
          this.es_wd = value;
        }
      }

      // let tempWeek: any[] = [];
      this.week[0] = this.user.Maandag;
      this.week[1] = this.user.Dinsdag;
      this.week[2] = this.user.Woensdag;
      this.week[3] = this.user.Donderdag;
      this.week[4] = this.user.Vrijdag;
      this.week[5] = this.user.Zaterdag;
      this.week[6] = this.user.Zondag;

      // Gender
      if (this.user.gender === 'Female') {
        this.user.gender = 'Vrouw';
      }
      if (this.user.gender === 'Couple') {
        this.user.gender = 'Stel';
      }

      console.log(this.user)
    });
  }

  close() {
    this.dialogRef.close();
  }

  async getUserReviews() {
    let b = 0;
    let h = 0;
    let l = 0;

    this.updateReviewControl();

    this.userDataService.getUserReviews(this.uid).snapshotChanges().pipe(
      map(changes =>
        changes.map(c =>
          ({ id: c.payload.doc.id, ...c.payload.doc.data() })
        )
      )
    ).subscribe(data => {
      for (let i = 0; i < data.length; i++) {
        this.userReviews[i] = data[i];

        this.userReviews = this.userReviews.sort((a, b) => {
          return <any>new Date(b.datum) - <any>new Date(a.datum);
        })

        this.userDataService.getUser(this.userReviews[i].id).valueChanges().subscribe(val => {
          this.userReviewers[i] = val;
        })
      }

      for (let i = 0; i < this.userReviews.length; i++) {
        this.userReviews[i].review = this.sanitizer.bypassSecurityTrustHtml(this.userReviews[i].review);
        
        b = b + this.userReviews[i].score;
        h = h + this.userReviews[i].hygene;
        l = l + this.userReviews[i].location;

        this.scorePercentage[i] = (20 * this.userReviews[i].score) + '%';
      }

      this.average = (b / this.userReviews.length).toFixed(1);
      this.hygeneAverage = (h / this.userReviews.length).toFixed(1);
      this.locationAverage = (l / this.userReviews.length).toFixed(1);

      this.averagePercentage = (20 * (b / this.userReviews.length)) + '%';
      this.hygenePercentage = (20 * (h / this.userReviews.length)) + '%';
      this.locationPercentage = (20 * (l / this.userReviews.length)) + '%';
    });
    console.log(this.userReviews);
  }

  async updateReviewControl(status?: boolean) {
    this.statusReview = false;
    this.blockReview = true;
    if (sessionStorage.getItem('user')) {
      this.userAuth = JSON.parse(sessionStorage.user);
      if (this.userAuth) {
        this.userDataService.getUser(this.userAuth.uid).valueChanges().subscribe(val => {
          this.userAuth = val;
          if (this.userAuth.accType === "2") {
            // this.statusReview = status ? status : false;
            this.blockReview = false;
          }
        });
      }
    }
  }

  getUserImgFiles(uid: string): void {
    if (this.user != undefined) {
      this.uploadImgService.getFiles(uid, 20).snapshotChanges().pipe(
        map(changes =>
          // store the key
          changes.map((c: { payload: { key: any; val: () => any; }; }) => ({ key: c.payload.key, ...c.payload.val() }))
        )
      ).subscribe((fileUploads: any) => {
        if (fileUploads.length != 0) {
          this.user.photoURL = fileUploads?.[0].url;

          for (let i = 0; i < fileUploads.length; i++) {
            this.userImgFiles[i] = [];
            this.userImgFiles[i] = fileUploads?.[i].url;
            this.imgArr[i] = fileUploads?.[i].url;
          }
        }
      });
    }
  }

  getUserVidFiles(uid: string): void {
    if (this.user != undefined) {
      this.uploadVidService.getFiles(uid, 20).snapshotChanges().pipe(
        map(changes =>
          // store the key
          changes.map((c: { payload: { key: any; val: () => any; }; }) => ({ key: c.payload.key, ...c.payload.val() }))
        )
      ).subscribe((fileUploads: any) => {
        for (let i = 0; i < fileUploads.length; i++) {
          this.userVidFiles[i] = [];
          this.userVidFiles[i] = fileUploads?.[i].url;
        }
      });
    }
  }

  showMobile() {
    this.flipped = !this.flipped;

    if (this.flipped) {
      this.btnVal = this.user.mobNumber;
    } else {
      this.btnVal = "Toon nummer";
    }
  }

  openDialog(activeSlide: string): void {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.data = [];
    dialogConfig.data[0] = this.imgArr;
    dialogConfig.data[1] = activeSlide;
    this.dialog.open(DialogBodyComponent, dialogConfig);
  }

  openDialog1(data:any): void {
    this.dialog.open(data);
  }

  onClick(event: { target: any; }) {
    var target = event?.target;
    var activeSlide = target.attributes.name.value;
    this.openDialog(activeSlide);
  }

  onClick1(event: { target: any; }, uid:string) {
    this.userDataService.childUid = uid;
    // var target = event?.target;
    this.openDialog1(EmployeeDetailsComponent);
  }

  onClick2(service:string, rating: number): void {
    let idx = this.servs.findIndex(elem => elem.id === service);
    let tempRating = 0;
    let u = 0;

    if(idx !== -1){
      this.servs[idx].rating = rating;
    }
    
    for(let i = 0; i < this.servs.length; i++) {
      tempRating = tempRating + this.servs[i].rating;
      if(this.servs[i].rating !== 0) {
        u++;
      }
    }

    this.rating = (tempRating / u);
  }

  onClickH(hygene:string, rating: number): void {
    this.hygene = rating;
  }
  
  onClickL(location:string, rating: number): void {
    this.location = rating;
  }

  showHideEvent() {
    this.status = !this.status;

    if (this.status === true) {
      this.showStatus = "Minder tonen";
    } else {
      this.showStatus = "Meer tonen";
    }
  }

  showHideEvent2() {
    this.statusReview = !this.statusReview;
    // this.updateReviewControl(this.statusReview);
  }

  selectedReview(data: number) {
    this.selectedIndex = (this.selectedIndex !== data) ? data : -9999;
  }

  async UpdateReview() {
    if (sessionStorage.getItem('user')) {
      this.userAuth = JSON.parse(sessionStorage.user);
      if (this.userAuth) {
        if( this.location && this.hygene){
          this.userDataService.createReview(this.uid, {
            datum: Date.now(),
            displayName: this.userAuth.displayName,
            review: this.html,
            reviewer: this.userAuth.uid,
            score: this.rating,
            revs: this.servs,
            hygene: this.hygene,
            location: this.location
          }).then(async () => {
            this.statusReview = false;
            this.html= "";
            this.location = 0;
            this.hygene = 0;
            this.rating = 0;
            this.servs= [];
            await this.userDataService.delay(250);
            this.ngOnInit();
          }
          ).catch((error: any) => {
            window.alert(error.message)
          });
        } else {
          window.alert("Hygiëne en/of Locatie zijn niet ingevuld, graag deze invullen.");
        }
      }
    }
  }

  
}

function context(context: any, isSecureContext: boolean, review: any): any {
  throw new Error('Function not implemented.');
}