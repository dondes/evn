import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EmpolyeeFormComponent } from './empolyee-form.component';

describe('EmpolyeeFormComponent', () => {
  let component: EmpolyeeFormComponent;
  let fixture: ComponentFixture<EmpolyeeFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EmpolyeeFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EmpolyeeFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
