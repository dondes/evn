import { Component, OnInit } from '@angular/core';
import { UserDataService } from '../../shared/services/user-data.service';
import { Editor, Toolbar } from 'ngx-editor';
import { MatDialogRef } from "@angular/material/dialog";

@Component({
  selector: 'app-empolyee-form',
  templateUrl: './empolyee-form.component.html',
  styleUrls: ['./empolyee-form.component.less']
})
export class EmpolyeeFormComponent implements OnInit {

  emptyText = '';

  editor: Editor = new Editor ;

  toolbar: Toolbar = [
    ["bold", "italic"],
    ["underline", "strike"],
    ["ordered_list", "bullet_list"],
  ];

  // provincies en gemeentes
  prov_gems = this.userDataService.prov_gems;

  // gender
  gender = this.userDataService.gender;

  // breast size
  breast_size = this.userDataService.breast_size;

  // origins
  origins = this.userDataService.origins;

  // Languages
  languages = this.userDataService.languages;

  // hair colors
  hair_color = this.userDataService.hair_color;

  // Body types
  body_type = this.userDataService.body_type;

  // Date preferences
  date_preference = this.userDataService.date_preference;

  // Sexual preferences
  sexualPref = this.userDataService.sexualPref;

  // Services
  dateServices = this.userDataService.dateServices;

  // Prices
  prices_po = this.userDataService.prices_po;
  
  prices_es = this.userDataService.prices_es;
  
  // Availability
  availability = this.userDataService.availability;

  genderContent: string = '';

  dateTypeContent: string = '';

  curUserData: any;

  parent_data: any;

  childNum: number = 0;
  
  constructor(public userDataService : UserDataService, public dialogRef: MatDialogRef<EmpolyeeFormComponent>) { }

  async ngOnInit(): Promise<void> {
    this.curUserData = JSON.parse(sessionStorage.user);

    await this.getUserData();
    
  }

  async getUserData() {
    this.userDataService.getUser(this.curUserData.uid).valueChanges().subscribe(val => {
      this.parent_data = val;
    });
  }

  radioButtonChanged($event: { source: { name: any; }; value: string; }) {
    // if you need the event source and value it can be accessed.
    // console.log($event.source.name, '---'+$event.value);

    switch ($event.value) {
      case 'Female':
        this.genderContent = $event.value;
        this.userDataService.clearInputMethod();
        break;
      case 'Male':
        this.genderContent = $event.value;
        this.userDataService.clearInputMethod();
        break;
      case 'Shemale':
        this.genderContent = $event.value;
        this.userDataService.clearInputMethod();
        break;
      case 'Couple':
        this.genderContent = $event.value;
        this.userDataService.clearInputMethod();
        break;
    }
  }

  async onSubmit() {

    this.childNum = this.parent_data?.childNum + 1;

    let data = this.userDataService.emptyForm.value;
    
    data.childOf = this.curUserData.uid;

    data.uid = this.parent_data?.uid +'_'+this.childNum;
    data.accType = "1";
    data.creationDate = Date.now();
    data.provGem = this.parent_data.provGem;
    
    this.parent_data.childNum = this.childNum;

    this.parent_data?.parentOf.push(data.uid);

    if(data.Maandag.hd === false ) {
      if((data.Maandag.bt.length == 0) && (data.Maandag.et.length == 0)){
        data.Maandag.nb = true;
        data.Maandag.bt = '';
        data.Maandag.et = '';
      }
    } else {
      data.Maandag.nb = false;
      data.Maandag.bt = '';
      data.Maandag.et = '';
    }

    if(data.Dinsdag.hd === false ) {
      if((data.Dinsdag.bt.length == 0) && (data.Dinsdag.et.length == 0)){
        data.Dinsdag.nb = true;
        data.Dinsdag.bt = '';
        data.Dinsdag.et = '';
        data.Dinsdag.hd = false;
      }
    } else {
      data.Dinsdag.nb = false;
      data.Dinsdag.bt = '';
      data.Dinsdag.et = '';
    }
    
    if(data.Woensdag.hd === false ) {
      if((data.Woensdag.bt.length == 0) && (data.Woensdag.et.length == 0)){
        data.Woensdag.nb = true;
        data.Woensdag.bt = '';
        data.Woensdag.et = '';
        data.Woensdag.hd = false;
      }
    } else {
      data.Woensdag.nb = false;
      data.Woensdag.bt = '';
      data.Woensdag.et = '';
    }
    
    if(data.Donderdag.hd === false ) {
      if((data.Donderdag.bt.length == 0) && (data.Donderdag.et.length == 0)){
        data.Donderdag.nb = true;
        data.Donderdag.bt = '';
        data.Donderdag.et = '';
        data.Donderdag.hd = false;
      }
    } else {
      data.Donderdag.nb = false;
      data.Donderdag.bt = '';
      data.Donderdag.et = '';
    }

    if(data.Vrijdag.hd === false ) {
      if((data.Vrijdag.bt.length == 0) && (data.Vrijdag.et.length == 0)){
        data.Vrijdag.nb = true;
        data.Vrijdag.bt = '';
        data.Vrijdag.et = '';
        data.Vrijdag.hd = false;
      }
    } else {
      data.Vrijdag.nb = false;
      data.Vrijdag.bt = '';
      data.Vrijdag.et = '';
    }

    if(data.Zaterdag.hd === false ) {
      if((data.Zaterdag.bt.length == 0) && (data.Zaterdag.et.length == 0)){
        data.Zaterdag.nb = true;
        data.Zaterdag.bt = '';
        data.Zaterdag.et = '';
        data.Zaterdag.hd = false;
      }
    } else {
      data.Zaterdag.nb = false;
      data.Zaterdag.bt = '';
      data.Zaterdag.et = '';
    }

    if(data.Zondag.hd === false ) {
      if((data.Zondag.bt.length == 0) && (data.Zondag.et.length == 0)){
        data.Zondag.nb = true;
        data.Zondag.bt = '';
        data.Zondag.et = '';
        data.Zondag.hd = false;
      }
    } else {
      data.Zondag.nb = false;
      data.Zondag.bt = '';
      data.Zondag.et = '';
    }
    
    await this.userDataService.updateUserData(this.parent_data, this.parent_data.uid, this.parent_data.updated);
    await this.userDataService.createEmployee(data);
    // this.userDataService.createUserData(data).then(res =>{
    //       /*do something here....
    //          maybe clear the form or give a success message*/
    // });



    // this.refreshPage();
  }

  close() {
    this.dialogRef.close();
  }
}
