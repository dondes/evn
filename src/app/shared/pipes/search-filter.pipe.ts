import { Pipe, PipeTransform } from '@angular/core';
import { User } from '../services/user';

@Pipe({
  name: 'searchFilter',
  pure: false
})
export class SearchFilterPipe implements PipeTransform {

  transform(list: any, filterText: string): any[] {
    filterText = filterText ? filterText.toLocaleLowerCase() : '';
    return filterText ? list.filter((user: User) =>
      user.displayName.toLocaleLowerCase().indexOf(filterText) !== -1) : list;
  }

}
