import { Injectable } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from '@angular/fire/firestore';
import { User, userReviews } from './user';
import { AuthService } from './auth.service';
import { snapshotChanges } from '@angular/fire/database';

@Injectable({
  providedIn: 'root'
})

export class UserDataService {
  public childUid = '';

  public uid = '';

  public allUsers?: User[];

  user?: User[];

  private dbPath = `/users`;
  private dbPathReviews = `/reviews`;

  constructor(private af: AngularFirestore, private authService: AuthService) {
    this.usersRef = af.collection(this.dbPath);
    this.reviewsRef = af.collection(this.dbPathReviews);
  }

  // all users database ref
  usersRef: AngularFirestoreCollection<User>;

  // reviews database ref
  reviewsRef: AngularFirestoreCollection<userReviews>;

  form = new FormGroup({
    // UID: new FormControl(''),
    displayName: new FormControl(''),
    provGem: new FormGroup({
      gem: new FormControl(''),
      prov: new FormControl('')
    }),
    lengte: new FormControl(''),
    mobNumber: new FormControl(''),
    introTitle: new FormControl(''),
    info: new FormControl(''),
    age: new FormControl(0),
    gender: new FormControl(''),
    origin: new FormControl(''),
    languages: new FormGroup({
      ned: new FormControl(false),
      eng: new FormControl(false),
      dui: new FormControl(false),
      fra: new FormControl(false),
      spa: new FormControl(false),
    }),
    hairColor: new FormControl(''),
    bodyType: new FormControl(''),
    datePrefs: new FormGroup({
      es: new FormControl(false),
      bd: new FormControl(false),
      gb: new FormControl(false),
      rp: new FormControl(false),
      po: new FormControl(false),
      em: new FormControl(false)
    }),
    sexualPref: new FormControl(''),
    dateServices: new FormGroup({
      abk: new FormControl(false),
      bis: new FormControl(false),
      bus: new FormControl(false),
      gan: new FormControl(false),
      kim: new FormControl(false),
      kog: new FormControl(false),
      pdk: new FormControl(false),
      pzc: new FormControl(false),
      tbr: new FormControl(false),
      bko: new FormControl(false),
      tmm: new FormControl(false),
      tmv: new FormControl(false),
      zoe: new FormControl(false),
      mmh: new FormControl(false),
      bef: new FormControl(false),
      int: new FormControl(false),
      kbk: new FormControl(false),
      szc: new FormControl(false),
      bkd: new FormControl(false),
      hic: new FormControl(false),
      ssm: new FormControl(false),
      pij: new FormControl(false),
      spk: new FormControl(false),
      ton: new FormControl(false),
      pla: new FormControl(false),
      prm: new FormControl(false),
      vin: new FormControl(false),
      ana: new FormControl(false),
      pdt: new FormControl(false),
      squ: new FormControl(false),
      fis: new FormControl(false),
      fbk: new FormControl(false),
      aft: new FormControl(false),
      pem: new FormControl(false),
      asw: new FormControl(false),
      bon: new FormControl(false),
      bhh: new FormControl(false),
      brp: new FormControl(false),
      can: new FormControl(false),
      cab: new FormControl(false),
      evw: new FormControl(false),
      gag: new FormControl(false),
      gos: new FormControl(false),
      kug: new FormControl(false),
      lar: new FormControl(false),
      mas: new FormControl(false),
      mum: new FormControl(false),
      naa: new FormControl(false),
      nit: new FormControl(false),
      nur: new FormControl(false),
      ops: new FormControl(false),
      put: new FormControl(false),
      sou: new FormControl(false),
      spi: new FormControl(false),
      tra: new FormControl(false),
      veh: new FormControl(false),
      ver: new FormControl(false),
      wor: new FormControl(false),
      vos: new FormControl(false),
      vio: new FormControl(false),
    }),
    dateServicesExtra: new FormGroup({
      abk_extra: new FormControl(0),
      bis_extra: new FormControl(0),
      bus_extra: new FormControl(0),
      gan_extra: new FormControl(0),
      kim_extra: new FormControl(0),
      kog_extra: new FormControl(0),
      pdk_extra: new FormControl(0),
      pzc_extra: new FormControl(0),
      tbr_extra: new FormControl(0),
      bko_extra: new FormControl(0),
      tmm_extra: new FormControl(0),
      tmv_extra: new FormControl(0),
      zoe_extra: new FormControl(0),
      mmh_extra: new FormControl(0),
      bef_extra: new FormControl(0),
      int_extra: new FormControl(0),
      kbk_extra: new FormControl(0),
      szc_extra: new FormControl(0),
      bkd_extra: new FormControl(0),
      hic_extra: new FormControl(0),
      ssm_extra: new FormControl(0),
      pij_extra: new FormControl(0),
      spk_extra: new FormControl(0),
      ton_extra: new FormControl(0),
      pla_extra: new FormControl(0),
      prm_extra: new FormControl(0),
      vin_extra: new FormControl(0),
      ana_extra: new FormControl(0),
      pdt_extra: new FormControl(0),
      squ_extra: new FormControl(0),
      fis_extra: new FormControl(0),
      fbk_extra: new FormControl(0),
      aft_extra: new FormControl(0),
      pem_extra: new FormControl(0),
      asw_extra: new FormControl(0),
      bon_extra: new FormControl(0),
      bhh_extra: new FormControl(0),
      brp_extra: new FormControl(0),
      can_extra: new FormControl(0),
      cab_extra: new FormControl(0),
      evw_extra: new FormControl(0),
      gag_extra: new FormControl(0),
      gos_extra: new FormControl(0),
      kug_extra: new FormControl(0),
      lar_extra: new FormControl(0),
      mas_extra: new FormControl(0),
      mum_extra: new FormControl(0),
      naa_extra: new FormControl(0),
      nit_extra: new FormControl(0),
      nur_extra: new FormControl(0),
      ops_extra: new FormControl(0),
      put_extra: new FormControl(0),
      sou_extra: new FormControl(0),
      spi_extra: new FormControl(0),
      tra_extra: new FormControl(0),
      veh_extra: new FormControl(0),
      ver_extra: new FormControl(0),
      wor_extra: new FormControl(0),
      vos_extra: new FormControl(0),
      vio_extra: new FormControl(0),
    }),
    poPrices: new FormGroup({
      qy: new FormControl(0),
      hu: new FormControl(0),
      dk: new FormControl(0),
      eu: new FormControl(0),
      tu: new FormControl(0),
      vu: new FormControl(0)
    }),
    esPrices: new FormGroup({
      eu: new FormControl(0),
      tu: new FormControl(0),
      vu: new FormControl(0),
      au: new FormControl(0),
      hd: new FormControl(0),
      wd: new FormControl(0)
    }),
    Maandag: new FormGroup({
      bt: new FormControl(''),
      et: new FormControl(''),
      hd: new FormControl(false),
      nb: new FormControl(false)
    }),
    Dinsdag: new FormGroup({
      bt: new FormControl(''),
      et: new FormControl(''),
      hd: new FormControl(false),
      nb: new FormControl(false)
    }),
    Woensdag: new FormGroup({
      bt: new FormControl(''),
      et: new FormControl(''),
      hd: new FormControl(false),
      nb: new FormControl(false)
    }),
    Donderdag: new FormGroup({
      bt: new FormControl(''),
      et: new FormControl(''),
      hd: new FormControl(false),
      nb: new FormControl(false)
    }),
    Vrijdag: new FormGroup({
      bt: new FormControl(''),
      et: new FormControl(''),
      hd: new FormControl(false),
      nb: new FormControl(false)
    }),
    Zaterdag: new FormGroup({
      bt: new FormControl(''),
      et: new FormControl(''),
      hd: new FormControl(false),
      nb: new FormControl(false)
    }),
    Zondag: new FormGroup({
      bt: new FormControl(''),
      et: new FormControl(''),
      hd: new FormControl(false),
      nb: new FormControl(false)
    }),
    published: new FormControl(false),
    street: new FormControl(''),
    houseNr: new FormControl(''),
    addative: new FormControl(''),
    zipcode: new FormControl(''),
    phoneNumber: new FormControl(''),
    parentOf: new FormControl(''),
    childOf: new FormControl(''),
  })

  form1 = new FormGroup({
    displayName: new FormControl(''),
  })

  form2 = new FormGroup({
    displayName: new FormControl(''),
    age: new FormControl(0),
    gender: new FormControl(''),
  })

  emptyForm = new FormGroup({
    // uid: new FormControl(''),
    displayName: new FormControl(''),
    provGem: new FormGroup({
      gem: new FormControl(''),
      prov: new FormControl('')
    }),
    lengte: new FormControl(''),
    mobNumber: new FormControl(''),
    introTitle: new FormControl(''),
    info: new FormControl(''),
    age: new FormControl(0),
    gender: new FormControl(''),
    origin: new FormControl(''),
    languages: new FormGroup({
      ned: new FormControl(false),
      eng: new FormControl(false),
      dui: new FormControl(false),
      fra: new FormControl(false),
      spa: new FormControl(false),
    }),
    hairColor: new FormControl(''),
    bodyType: new FormControl(''),
    datePrefs: new FormGroup({
      es: new FormControl(false),
      bd: new FormControl(false),
      gb: new FormControl(false),
      rp: new FormControl(false),
      po: new FormControl(false),
      em: new FormControl(false)
    }),
    sexualPref: new FormControl(''),
    dateServices: new FormGroup({
      abk: new FormControl(false),
      bis: new FormControl(false),
      bus: new FormControl(false),
      gan: new FormControl(false),
      kim: new FormControl(false),
      kog: new FormControl(false),
      pdk: new FormControl(false),
      pzc: new FormControl(false),
      tbr: new FormControl(false),
      bko: new FormControl(false),
      tmm: new FormControl(false),
      tmv: new FormControl(false),
      zoe: new FormControl(false),
      mmh: new FormControl(false),
      bef: new FormControl(false),
      int: new FormControl(false),
      kbk: new FormControl(false),
      szc: new FormControl(false),
      bkd: new FormControl(false),
      hic: new FormControl(false),
      ssm: new FormControl(false),
      pij: new FormControl(false),
      spk: new FormControl(false),
      ton: new FormControl(false),
      pla: new FormControl(false),
      prm: new FormControl(false),
      vin: new FormControl(false),
      ana: new FormControl(false),
      pdt: new FormControl(false),
      squ: new FormControl(false),
      fis: new FormControl(false),
      fbk: new FormControl(false),
      aft: new FormControl(false),
      pem: new FormControl(false),
      asw: new FormControl(false),
      bon: new FormControl(false),
      bhh: new FormControl(false),
      brp: new FormControl(false),
      can: new FormControl(false),
      cab: new FormControl(false),
      evw: new FormControl(false),
      gag: new FormControl(false),
      gos: new FormControl(false),
      kug: new FormControl(false),
      lar: new FormControl(false),
      mas: new FormControl(false),
      mum: new FormControl(false),
      naa: new FormControl(false),
      nit: new FormControl(false),
      nur: new FormControl(false),
      ops: new FormControl(false),
      put: new FormControl(false),
      sou: new FormControl(false),
      spi: new FormControl(false),
      tra: new FormControl(false),
      veh: new FormControl(false),
      ver: new FormControl(false),
      wor: new FormControl(false),
      vos: new FormControl(false),
      vio: new FormControl(false),
    }),
    dateServicesExtra: new FormGroup({
      abk_extra: new FormControl(0),
      bis_extra: new FormControl(0),
      bus_extra: new FormControl(0),
      gan_extra: new FormControl(0),
      kim_extra: new FormControl(0),
      kog_extra: new FormControl(0),
      pdk_extra: new FormControl(0),
      pzc_extra: new FormControl(0),
      tbr_extra: new FormControl(0),
      bko_extra: new FormControl(0),
      tmm_extra: new FormControl(0),
      tmv_extra: new FormControl(0),
      zoe_extra: new FormControl(0),
      mmh_extra: new FormControl(0),
      bef_extra: new FormControl(0),
      int_extra: new FormControl(0),
      kbk_extra: new FormControl(0),
      szc_extra: new FormControl(0),
      bkd_extra: new FormControl(0),
      hic_extra: new FormControl(0),
      ssm_extra: new FormControl(0),
      pij_extra: new FormControl(0),
      spk_extra: new FormControl(0),
      ton_extra: new FormControl(0),
      pla_extra: new FormControl(0),
      prm_extra: new FormControl(0),
      vin_extra: new FormControl(0),
      ana_extra: new FormControl(0),
      pdt_extra: new FormControl(0),
      squ_extra: new FormControl(0),
      fis_extra: new FormControl(0),
      fbk_extra: new FormControl(0),
      aft_extra: new FormControl(0),
      pem_extra: new FormControl(0),
      asw_extra: new FormControl(0),
      bon_extra: new FormControl(0),
      bhh_extra: new FormControl(0),
      brp_extra: new FormControl(0),
      can_extra: new FormControl(0),
      cab_extra: new FormControl(0),
      evw_extra: new FormControl(0),
      gag_extra: new FormControl(0),
      gos_extra: new FormControl(0),
      kug_extra: new FormControl(0),
      lar_extra: new FormControl(0),
      mas_extra: new FormControl(0),
      mum_extra: new FormControl(0),
      naa_extra: new FormControl(0),
      nit_extra: new FormControl(0),
      nur_extra: new FormControl(0),
      ops_extra: new FormControl(0),
      put_extra: new FormControl(0),
      sou_extra: new FormControl(0),
      spi_extra: new FormControl(0),
      tra_extra: new FormControl(0),
      veh_extra: new FormControl(0),
      ver_extra: new FormControl(0),
      wor_extra: new FormControl(0),
      vos_extra: new FormControl(0),
      vio_extra: new FormControl(0),
    }),
    poPrices: new FormGroup({
      qy: new FormControl(0),
      hu: new FormControl(0),
      dk: new FormControl(0),
      eu: new FormControl(0),
      tu: new FormControl(0),
      vu: new FormControl(0)
    }),
    esPrices: new FormGroup({
      eu: new FormControl(0),
      tu: new FormControl(0),
      vu: new FormControl(0),
      au: new FormControl(0),
      hd: new FormControl(0),
      wd: new FormControl(0)
    }),
    Maandag: new FormGroup({
      bt: new FormControl(''),
      et: new FormControl(''),
      hd: new FormControl(false),
      nb: new FormControl(false)
    }),
    Dinsdag: new FormGroup({
      bt: new FormControl(''),
      et: new FormControl(''),
      hd: new FormControl(false),
      nb: new FormControl(false)
    }),
    Woensdag: new FormGroup({
      bt: new FormControl(''),
      et: new FormControl(''),
      hd: new FormControl(false),
      nb: new FormControl(false)
    }),
    Donderdag: new FormGroup({
      bt: new FormControl(''),
      et: new FormControl(''),
      hd: new FormControl(false),
      nb: new FormControl(false)
    }),
    Vrijdag: new FormGroup({
      bt: new FormControl(''),
      et: new FormControl(''),
      hd: new FormControl(false),
      nb: new FormControl(false)
    }),
    Zaterdag: new FormGroup({
      bt: new FormControl(''),
      et: new FormControl(''),
      hd: new FormControl(false),
      nb: new FormControl(false)
    }),
    Zondag: new FormGroup({
      bt: new FormControl(''),
      et: new FormControl(''),
      hd: new FormControl(false),
      nb: new FormControl(false)
    }),
    published: new FormControl(false),
    street: new FormControl(''),
    houseNr: new FormControl(''),
    addative: new FormControl(''),
    zipcode: new FormControl(''),
    phoneNumber: new FormControl(''),
    parentOf: new FormControl(''),
    childOf: new FormControl(''),
  })

  form_review = new FormGroup({
    reviewer: new FormControl(''),
    review: new FormControl(''),
    date: new FormControl(Date),
    rating: new FormControl(0)
  })

  childForm = new FormGroup({
    // UID: new FormControl(''),
    displayName: new FormControl(''),
    provGem: new FormGroup({
      gem: new FormControl(''),
      prov: new FormControl('')
    }),
    lengte: new FormControl(''),
    mobNumber: new FormControl(''),
    introTitle: new FormControl(''),
    info: new FormControl(''),
    age: new FormControl(0),
    gender: new FormControl(''),
    origin: new FormControl(''),
    languages: new FormGroup({
      ned: new FormControl(false),
      eng: new FormControl(false),
      dui: new FormControl(false),
      fra: new FormControl(false),
      spa: new FormControl(false),
    }),
    hairColor: new FormControl(''),
    bodyType: new FormControl(''),
    datePrefs: new FormGroup({
      es: new FormControl(false),
      bd: new FormControl(false),
      gb: new FormControl(false),
      rp: new FormControl(false),
      po: new FormControl(false),
      em: new FormControl(false)
    }),
    sexualPref: new FormControl(''),
    dateServices: new FormGroup({
      abk: new FormControl(false),
      bis: new FormControl(false),
      bus: new FormControl(false),
      gan: new FormControl(false),
      kim: new FormControl(false),
      kog: new FormControl(false),
      pdk: new FormControl(false),
      pzc: new FormControl(false),
      tbr: new FormControl(false),
      bko: new FormControl(false),
      tmm: new FormControl(false),
      tmv: new FormControl(false),
      zoe: new FormControl(false),
      mmh: new FormControl(false),
      bef: new FormControl(false),
      int: new FormControl(false),
      kbk: new FormControl(false),
      szc: new FormControl(false),
      bkd: new FormControl(false),
      hic: new FormControl(false),
      ssm: new FormControl(false),
      pij: new FormControl(false),
      spk: new FormControl(false),
      ton: new FormControl(false),
      pla: new FormControl(false),
      prm: new FormControl(false),
      vin: new FormControl(false),
      ana: new FormControl(false),
      pdt: new FormControl(false),
      squ: new FormControl(false),
      fis: new FormControl(false),
      fbk: new FormControl(false),
      aft: new FormControl(false),
      pem: new FormControl(false),
      asw: new FormControl(false),
      bon: new FormControl(false),
      bhh: new FormControl(false),
      brp: new FormControl(false),
      can: new FormControl(false),
      cab: new FormControl(false),
      evw: new FormControl(false),
      gag: new FormControl(false),
      gos: new FormControl(false),
      kug: new FormControl(false),
      lar: new FormControl(false),
      mas: new FormControl(false),
      mum: new FormControl(false),
      naa: new FormControl(false),
      nit: new FormControl(false),
      nur: new FormControl(false),
      ops: new FormControl(false),
      put: new FormControl(false),
      sou: new FormControl(false),
      spi: new FormControl(false),
      tra: new FormControl(false),
      veh: new FormControl(false),
      ver: new FormControl(false),
      wor: new FormControl(false),
      vos: new FormControl(false),
      vio: new FormControl(false),
    }),
    dateServicesExtra: new FormGroup({
      abk_extra: new FormControl(0),
      bis_extra: new FormControl(0),
      bus_extra: new FormControl(0),
      gan_extra: new FormControl(0),
      kim_extra: new FormControl(0),
      kog_extra: new FormControl(0),
      pdk_extra: new FormControl(0),
      pzc_extra: new FormControl(0),
      tbr_extra: new FormControl(0),
      bko_extra: new FormControl(0),
      tmm_extra: new FormControl(0),
      tmv_extra: new FormControl(0),
      zoe_extra: new FormControl(0),
      mmh_extra: new FormControl(0),
      bef_extra: new FormControl(0),
      int_extra: new FormControl(0),
      kbk_extra: new FormControl(0),
      szc_extra: new FormControl(0),
      bkd_extra: new FormControl(0),
      hic_extra: new FormControl(0),
      ssm_extra: new FormControl(0),
      pij_extra: new FormControl(0),
      spk_extra: new FormControl(0),
      ton_extra: new FormControl(0),
      pla_extra: new FormControl(0),
      prm_extra: new FormControl(0),
      vin_extra: new FormControl(0),
      ana_extra: new FormControl(0),
      pdt_extra: new FormControl(0),
      squ_extra: new FormControl(0),
      fis_extra: new FormControl(0),
      fbk_extra: new FormControl(0),
      aft_extra: new FormControl(0),
      pem_extra: new FormControl(0),
      asw_extra: new FormControl(0),
      bon_extra: new FormControl(0),
      bhh_extra: new FormControl(0),
      brp_extra: new FormControl(0),
      can_extra: new FormControl(0),
      cab_extra: new FormControl(0),
      evw_extra: new FormControl(0),
      gag_extra: new FormControl(0),
      gos_extra: new FormControl(0),
      kug_extra: new FormControl(0),
      lar_extra: new FormControl(0),
      mas_extra: new FormControl(0),
      mum_extra: new FormControl(0),
      naa_extra: new FormControl(0),
      nit_extra: new FormControl(0),
      nur_extra: new FormControl(0),
      ops_extra: new FormControl(0),
      put_extra: new FormControl(0),
      sou_extra: new FormControl(0),
      spi_extra: new FormControl(0),
      tra_extra: new FormControl(0),
      veh_extra: new FormControl(0),
      ver_extra: new FormControl(0),
      wor_extra: new FormControl(0),
      vos_extra: new FormControl(0),
      vio_extra: new FormControl(0),
    }),
    poPrices: new FormGroup({
      qy: new FormControl(0),
      hu: new FormControl(0),
      dk: new FormControl(0),
      eu: new FormControl(0),
      tu: new FormControl(0),
      vu: new FormControl(0)
    }),
    esPrices: new FormGroup({
      eu: new FormControl(0),
      tu: new FormControl(0),
      vu: new FormControl(0),
      au: new FormControl(0),
      hd: new FormControl(0),
      wd: new FormControl(0)
    }),
    Maandag: new FormGroup({
      bt: new FormControl(''),
      et: new FormControl(''),
      hd: new FormControl(false),
      nb: new FormControl(false)
    }),
    Dinsdag: new FormGroup({
      bt: new FormControl(''),
      et: new FormControl(''),
      hd: new FormControl(false),
      nb: new FormControl(false)
    }),
    Woensdag: new FormGroup({
      bt: new FormControl(''),
      et: new FormControl(''),
      hd: new FormControl(false),
      nb: new FormControl(false)
    }),
    Donderdag: new FormGroup({
      bt: new FormControl(''),
      et: new FormControl(''),
      hd: new FormControl(false),
      nb: new FormControl(false)
    }),
    Vrijdag: new FormGroup({
      bt: new FormControl(''),
      et: new FormControl(''),
      hd: new FormControl(false),
      nb: new FormControl(false)
    }),
    Zaterdag: new FormGroup({
      bt: new FormControl(''),
      et: new FormControl(''),
      hd: new FormControl(false),
      nb: new FormControl(false)
    }),
    Zondag: new FormGroup({
      bt: new FormControl(''),
      et: new FormControl(''),
      hd: new FormControl(false),
      nb: new FormControl(false)
    }),
    published: new FormControl(false),
    street: new FormControl(''),
    houseNr: new FormControl(''),
    addative: new FormControl(''),
    zipcode: new FormControl(''),
    phoneNumber: new FormControl(''),
    parentOf: new FormControl(''),
    childOf: new FormControl(''),
  })

  //get user
  getUser(uid: string): AngularFirestoreDocument<User> {
    return this.af.doc(`/users/${uid}`);
  }

  // all users
  getAllData(): AngularFirestoreCollection<User> {
    return this.usersRef;
  }

  getAllReviews(): AngularFirestoreCollection<userReviews> {
    return this.reviewsRef;
  }

  // Create user
  createUserData(data: any) {
    return new Promise<any>((resolve, reject) => {
      this.af
        .collection("users")
        .add(data)
        .then(res => { }, err => (err));
    })
  }

  createEmployee(data:any) {
    return new Promise<any>((resolve, reject) => {
      this.af
        .collection("users")
        .doc(data.uid)
        .set(data)
        .then(res => { }, err => (err));
    })
  }

  deleteUserData(uid: string) {
    console.log(uid);
      this.af
        .collection("users")
        .doc(uid)
        .delete();
  }

  updateUserData(data: any, uid: string, updated: number) {
    console.log(data);
    return this.af
      .collection("users")
      .doc(uid)
      .update({
        displayName: data.displayName,
        provGem: data.provGem,
        mobNumber: data.mobNumber,
        introTitle: data.introTitle,
        info: data.info,
        lengte: data.lengte,
        age: data.age,
        gender: data.gender,
        origin: data.origin,
        languages: data.languages,
        hairColor: data.hairColor,
        bodyType: data.bodyType,
        datePrefs: data.datePrefs,
        sexualPref: data.sexualPref,
        dateServices: data.dateServices,
        dateServicesExtra: data.dateServicesExtra,
        poPrices: data.poPrices,
        esPrices: data.esPrices,
        Maandag: data.Maandag,
        Dinsdag: data.Dinsdag,
        Woensdag: data.Woensdag,
        Donderdag: data.Donderdag,
        Vrijdag: data.Vrijdag,
        Zaterdag: data.Zaterdag,
        Zondag: data.Zondag,
        published: data.published,
        updated: updated,
        street: data.street,
        houseNr: data.houseNr,
        addative: data.addative,
        zipcode: data.zipcode,
        phoneNumber: data.phoneNumber,
        parentOf:data.parentOf,
        childOf: data.childOf,
        childNum: data.childNum
      });
  }

  delay(ms: number) {
    return new Promise( resolve => setTimeout(resolve, ms) );
  }

  ///====================User Reviews====================

  updateUserDataReviewer(data: any, uid: string) {
    console.log(data);
    return this.af
      .collection("users")
      .doc(uid)
      .update({
        displayName: data.displayName
      });
  }

  getUserReviews(uid: string): AngularFirestoreCollection<userReviews> {
    // return this.afs.collection(`/reviews/${uid}/reviews`);
    return this.reviewsRef.doc(uid).collection('reviews');
  }

  getAllUserReviews(): AngularFirestoreCollection<userReviews> {
    // return this.afs.collection(`/reviews/${uid}/reviews`);
    return this.reviewsRef;
  }

  // async getAllCreatedReviews() {
  //   const snapshot = await this.af.collection('reviews').get()
  //   const collection = {};
  //   snapshot.forEach(doc => {
  //       collection[doc.id] = doc.data();
  //   });
  //   return collection;
  // }

  createReview(uid: string, review: any): any {
    return this.reviewsRef.doc(uid).collection('reviews').add({ ...review });
  }
  
  updateReview(uid: string, review: any): Promise<void> {
    return this.reviewsRef.doc(uid).update(review);
  }

  deleteReview(reviewdUid:any, datum:number) {
    let collectionRef = this.reviewsRef.doc(reviewdUid).collection('reviews', ref => ref.where('datum','==', datum)).get();
    console.log(collectionRef);
    collectionRef.toPromise()
    .then((querySnapshot) => {
      querySnapshot.forEach((doc) => {
        doc.ref.delete();
      })
    })
  }

  updateUserReviews(datum:number, reviewed:any, reviewer:any) {
    let collectionRef = this.reviewsRef.doc(reviewed).collection('reviews', ref => ref.where('datum','==', datum)).get();
    console.log(collectionRef);
    collectionRef.toPromise()
    .then((querySnapshot) => {
      querySnapshot.forEach((doc) => {
        doc.ref.update({displayName: reviewer});
      })
    })
  }

  get breastSize(): any { return this.form.get('breastSize'); }
  get penisSize(): any { return this.form.get('penisSize'); }
  
  clearInputMethod() {
    this.breastSize.reset();
    this.penisSize.reset();
  }

  
  // data arrays
  // ==============================================================================================================
  // provincies en gemeentes
  prov_gems = [
    {
      prov: "Drenthe",
      gems: ["Aa en Hunze", "Assen", "Borger-Odoorn", "Coevorden", "Emmen", "Hoogeveen", "Meppel", "Midden-Drenthe", "Noordenveld", "Tynaarlo", "Westerveld", "De Wolden"]
    },
    {
      prov: "Flevoland",
      gems: ["Almere", "Dronten", "Lelystad", "Noordoostpolder", "Urk", "Zeewolde"]
    },
    {
      prov: "Friesland",
      gems: ["Achtkarspelen", "Ameland", "Dantumadiel", "De Fryske Marren", "Harlingen", "Heerenveen", "Leeuwarden", "Noardeast-Fryslân", "Ooststellingwerf", "Opsterland", "Schiermonnikoog", "Smallingerland", "Súdwest-Fryslân", "Terschelling", "Tytsjerksteradiel", "Vlieland", "Waadhoeke", "Weststellingwerf"]
    },
    {
      prov: "Gelderland",
      gems: ["Aalten", "Apeldoorn", "Arnhem", "Barneveld", "Berg en Dal", "Berkelland", "Beuningen", "Bronckhorst", "Brummen", "Buren", "Culemborg", "Doesburg", "Doetinchem", "Druten", "Duiven", "Ede", "Elburg", "Epe", "Ermelo", "Harderwijk", "Hattem", "Heerde", "Heumen", "Lingewaard", "Lochem", "Maasdriel", "Montferland", "Neder-Betuwe", "Nijkerk", "Nijmegen", "Nunspeet", "Oldebroek", "Oost Gelre", "Oude IJsselstreek", "Overbetuwe", "Putten", "Renkum", "Rheden", "Rozendaal", "Scherpenzeel", "Tiel", "Voorst", "Wageningen", "West Betuwe", "West Maas en Waal", "Westervoort", "Wijchen", "Winterswijk", "Zaltbommel", "Zevenaar", "Zutphen"]
    },
    {
      prov: "Groningen",
      gems: ["Eemsdelta", "Groningen", "Het Hogeland", "Midden-Groningen", "Oldambt", "Pekela", "Stadskanaal", "Veendam", "Westerkwartier", "Westerwolde"]
    },
    {
      prov: "Limburg",
      gems: ["Beek", "Beekdaelen", "Beesel", "Bergen (LI)", "Brunssum", "Echt-Susteren", "Eijsden-Margraten", "Gennep", "Gulpen-Wittem", "Heerlen", "Horst aan de Maas", "Kerkrade", "Landgraaf", "Leudal", "Maasgouw", "Maastricht", "Meerssen", "Mook en Middelaar", "Nederweert", "Peel en Maas", "Roerdalen", "Roermond", "Simpelveld", "Sittard-Geleen", "Stein", "Vaals", "Valkenburg aan de Geul", "Venlo", "Venray", "Voerendaal", "Weert"]
    },
    {
      prov: "Noord-Brabant",
      gems: ["Alkmaar", "Alphen-Chaam", "Altena", "Asten", "Baarle-Nassau", "Bergeijk", "Bergen op Zoom", "Bernheze", "Best", "Bladel", "Boekel", "Boxmeer", "Boxtel", "Breda", "Cranendonck", "Cuijk", "Deurne", "Dongen", "Drimmelen", "Eersel", "Eindhoven", "Etten-Leur", "Geertruidenberg", "Geldrop-Mierlo", "Gemert-Bakel", "Gilze en Rijen", "Goirle", "Grave", "Halderberge", "Heeze-Leende", "Helmond", "'s-Hertogenbosch", "Heusden", "Hilvarenbeek", "Laarbeek", "Landerd", "Loon op Zand", "Meierijstad", "Mill en Sint Hubert", "Moerdijk", "Nuenen, Gerwen en Nederwetten", "Oirschot", "Oisterwijk", "Oosterhout", "Oss", "Reusel-De Mierden", "Roosendaal", "Rucphen", "Sint Anthonis", "Sint-Michielsgestel", "Someren", "Son en Breugel", "Steenbergen", "Tilburg", "Uden", "Valkenswaard", "Veldhoven", "Vught", "Waalre", "Waalwijk", "Woensdrecht", "Zundert"]
    },
    {
      prov: "Noord-Holland",
      gems: ["Aalsmeer", "Amstelveen", "Amsterdam", "Beemster", "Bergen (NH)", "Beverwijk", "Blaricum", "Bloemendaal", "Castricum", "Diemen", "Drechterland", "Edam-Volendam", "Enkhuizen", "Gooise Meren", "Haarlem", "Haarlemmermeer", "Heemskerk", "Heemstede", "Heerhugowaard", "Heiloo", "Den Helder", "Hilversum", "Hollands Kroon", "Hoorn", "Huizen", "Koggenland", "Landsmeer", "Langedijk", "Laren", "Medemblik", "Oostzaan", "Opmeer", "Ouder-Amstel", "Purmerend", "Schagen", "Stede Broec", "Texel", "Uitgeest", "Uithoorn", "Velsen", "Waterland", "Weesp", "Wijdemeren", "Wormerland", "Zaanstad", "Zandvoort"]
    },
    {
      prov: "Overijssel",
      gems: ["Almelo", "Borne", "Dalfsen", "Deventer", "Dinkelland", "Enschede", "Haaksbergen", "Hardenberg", "Hellendoorn", "Hengelo", "Hof van Twente", "Kampen", "Losser", "Oldenzaal", "Olst-Wijhe", "Ommen", "Raalte", "Rijssen-Holten", "Staphorst", "Steenwijkerland", "Tubbergen", "Twenterand", "Wierden", "Zwartewaterland", "Zwolle"]
    },
    {
      prov: "Utrecht",
      gems: ["Amersfoort", "Baarn", "De Bilt", "Bunnik", "Bunschoten", "Eemnes", "Houten", "IJsselstein", "Leusden", "Lopik", "Montfoort", "Nieuwegein", "Oudewater", "Renswoude", "Rhenen", "De Ronde Venen", "Soest", "Stichtse Vecht", "Utrecht", "Utrechtse Heuvelrug", "Veenendaal", "Vijfheerenlanden", "Wijk bij Duurstede", "Woerden", "Woudenberg", "Zeist"]
    },
    {
      prov: "Zeeland",
      gems: ["Borsele", "Goes", "Hulst", "Kapelle", "Middelburg", "Noord-Beveland", "Reimerswaal", "Schouwen-Duiveland", "Sluis", "Terneuzen", "Tholen", "Veere", "Vlissingen"]
    },
    {
      prov: "Zuid-Holland",
      gems: ["Alblasserdam", "Albrandswaard", "Alphen aan den Rijn", "Barendrecht", "Bodegraven-Reeuwijk", "Brielle", "Capelle aan den IJssel", "Delft", "Dordrecht", "Goeree-Overflakkee", "Gorinchem", "Gouda", "Den Haag", "Hardinxveld-Giessendam", "Hellevoetsluis", "Hendrik-Ido-Ambacht", "Hillegom", "Hoeksche Waard", "Kaag en Braassem", "Katwijk", "Krimpen aan den IJssel", "Krimpenerwaard", "Lansingerland", "Leiden", "Leiderdorp", "Leidschendam-Voorburg", "Lisse", "Maassluis", "Midden-Delfland", "Molenlanden", "Nieuwkoop", "Nissewaard", "Noordwijk", "Oegstgeest", "Papendrecht", "Pijnacker-Nootdorp", "Ridderkerk", "Rijswijk", "Rotterdam", "Schiedam", "Sliedrecht", "Teylingen", "Vlaardingen", "Voorschoten", "Waddinxveen", "Wassenaar", "Westland", "Westvoorne", "Zoetermeer", "Zoeterwoude", "Zuidplas", "Zwijndrecht"]
    }
  ]

  // gender
  gender = ['Female', 'Male', 'Shemale', 'Couple'];

  // breast size
  breast_size = ['AA', 'A', 'B', 'C', 'D', 'DD', 'E', 'F', 'FF', 'G', 'GG', 'H', 'HH', 'J', 'JJ'];

  // origins
  origins = ['Nederlands', 'Europees', 'Oost Europees', 'Zuid Europees', 'Afrikaans', 'Aziatisch', 'Amerikaans', 'Zuid Amerikaans', 'Arabisch', 'Turks', 'Marokkaans']

  // Languages
  languages = [
    {
      name: 'Nederlands',
      code: 'ned',
      id: 0
    },
    {
      name: 'Engels',
      code: 'eng',
      id: 1
    },
    {
      name: 'Duits',
      code: 'dui',
      id: 2
    },
    {
      name: 'Frans',
      code: 'fra',
      id: 3
    },
    {
      name: 'Spaans',
      code: 'spa',
      id: 4
    },
  ];

  // hair colors
  hair_color = ['Blond', 'Donkerblond', 'Burnette', 'Bruin', 'Wit', 'Grijs', 'Zwart', 'Anders'];

  // Body types
  body_type = ['Slank', 'Atletisch', 'Normaal', 'Mollig', 'Dik'];

  // Date preferences
  date_preference = [
    {
      name: 'Escort',
      code: 'es',
      id: 0
    },
    {
      name: 'BDSM',
      code: 'bd',
      id: 1
    },
    {
      name: 'Erotische massage',
      code: 'em',
      id: 2
    },
    {
      name: 'Gangbang',
      code: 'gb',
      id: 3
    },
    {
      name: 'Prive ontvangst',
      code: 'po',
      id: 4
    },
    {
      name: 'Raamprostitutie',
      code: 'rp',
      id: 5
    }
  ];

  // Sexual preferences
  sexualPref = ['Hetero', 'Biseksueel', 'Gay', 'Lesbisch'];

  // Services
  dateServices = [
    {
      name: 'Anaal bij klant',
      code: 'abk',
      id: 0
    },
    {
      name: 'Biseks',
      code: 'bis',
      id: 1
    },
    {
      name: 'Buiten seks',
      code: 'bus',
      id: 2
    },
    {
      name: 'Gangbang',
      code: 'gan',
      id: 3
    },
    {
      name: 'Klaarkomen in mond',
      code: 'kim',
      id: 4
    },
    {
      name: 'Klaarkomen op gezicht',
      code: 'kog',
      id: 5
    },
    {
      name: 'Plassex door klant',
      code: 'pdk',
      id: 6
    },
    {
      name: 'Pijpen zonder condoom',
      code: 'pzc',
      id: 7
    },
    {
      name: 'Tussen borsten (russisch)',
      code: 'tbr',
      id: 8
    },
    {
      name: 'BDSM klant onderdanig',
      code: 'bko',
      id: 9
    },
    {
      name: 'Trio m/m',
      code: 'tmm',
      id: 10
    },
    {
      name: 'Trio m/v',
      code: 'tmv',
      id: 11
    },
    {
      name: 'Zoenen',
      code: 'zoe',
      id: 12
    },
    {
      name: 'Massage met hoogtepunt',
      code: 'mmh',
      id: 13
    },
    {
      name: 'Beffen',
      code: 'bef',
      id: 14
    },
    {
      name: 'Intiem',
      code: 'int',
      id: 15
    },
    {
      name: 'Kontlikken bij klant',
      code: 'kbk',
      id: 16
    },
    {
      name: 'Sex zonder condoom',
      code: 'szc',
      id: 17
    },
    {
      name: 'BDSM klant dominant',
      code: 'bkd',
      id: 18
    },
    {
      name: 'Highclass companion',
      code: 'hic',
      id: 19
    },
    {
      name: 'Soft SM',
      code: 'ssm',
      id: 20
    },
    {
      name: 'Pijpen',
      code: 'pij',
      id: 21
    },
    {
      name: 'Speciale kledingverzoeken',
      code: 'spk',
      id: 22
    },
    {
      name: 'Tongzoenen',
      code: 'ton',
      id: 23
    },
    {
      name: 'Plassex',
      code: 'pla',
      id: 24
    },
    {
      name: 'Prostaat massage',
      code: 'prm',
      id: 25
    },
    {
      name: 'Vingeren',
      code: 'vin',
      id: 26
    },
    {
      name: 'Anaal',
      code: 'ana',
      id: 27
    },
    {
      name: 'Pijpen deepthroat',
      code: 'pdt',
      id: 28
    },
    {
      name: 'Squirting',
      code: 'squ',
      id: 29
    },
    {
      name: 'Fisting',
      code: 'fis',
      id: 30
    },
    {
      name: 'Fisting bij klant',
      code: 'fbk',
      id: 31
    },
    {
      name: 'Aftrekken',
      code: 'aft',
      id: 31
    },
    {
      name: 'Penismassage',
      code: 'pem',
      id: 33
    },
    {
      name: 'Ass worship',
      code: 'asw',
      id: 34
    },
    {
      name: 'Bondage',
      code: 'bon',
      id: 35
    },
    {
      name: 'Boot / High heel worship',
      code: 'bhh',
      id: 36
    },
    {
      name: 'Breath play',
      code: 'brp',
      id: 37
    },
    {
      name: 'Caning',
      code: 'can',
      id: 38
    },
    {
      name: 'Cock and Balls',
      code: 'cab',
      id: 39
    },
    {
      name: 'Electro / Violet wand',
      code: 'evw',
      id: 40
    },
    {
      name: 'Gagging / Gags',
      code: 'gag',
      id: 41
    },
    {
      name: 'Golden shower',
      code: 'gos',
      id: 42
    },
    {
      name: 'Kuisheidsgordel',
      code: 'kug',
      id: 43
    },
    {
      name: 'Lak / Rubber',
      code: 'lar',
      id: 44
    },
    {
      name: 'Masks',
      code: 'mas',
      id: 45
    },
    {
      name: 'Mummificatie',
      code: 'mum',
      id: 46
    },
    {
      name: 'Naalden',
      code: 'naa',
      id: 47
    },
    {
      name: 'Nipple torture',
      code: 'nit',
      id: 48
    },
    {
      name: 'Nursing',
      code: 'nur',
      id: 49
    },
    {
      name: 'Opsluiting',
      code: 'ops',
      id: 50
    },
    {
      name: 'Publieke tentoonstelling',
      code: 'put',
      id: 51
    },
    {
      name: 'Sounding',
      code: 'sou',
      id: 52
    },
    {
      name: 'Spitting',
      code: 'spi',
      id: 53
    },
    {
      name: 'Travestie',
      code: 'tra',
      id: 54
    },
    {
      name: 'Verbal humiliation',
      code: 'veh',
      id: 55
    },
    {
      name: 'Vernedering',
      code: 'ver',
      id: 56
    },
    {
      name: 'Worstelen',
      code: 'wor',
      id: 57
    },
    {
      name: 'Voorbinddildo sex',
      code: 'vos',
      id: 58
    },
    {
      name: 'Video opname',
      code: 'vio',
      id: 59
    },
  ]
  // // Services Extra
  // dateServicesExtra = [
  //   {
  //     name: 'Anaal bij klant',
  //     code: 'abk_extra',
  //     id: 0
  //   },
  //   {
  //     name: 'Biseks',
  //     code: 'bis_extra',
  //     id: 1
  //   },
  //   {
  //     name: 'Buiten seks',
  //     code: 'bus_extra',
  //     id: 2
  //   },
  //   {
  //     name: 'Gangbang',
  //     code: 'gan_extra',
  //     id: 3
  //   },
  //   {
  //     name: 'Klaarkomen in mond',
  //     code: 'kim',
  //     id: 4
  //   },
  //   {
  //     name: 'Klaarkomen op gezicht',
  //     code: 'kog_extra',
  //     id: 5
  //   },
  //   {
  //     name: 'Plassex door klant',
  //     code: 'pdk_extra',
  //     id: 6
  //   },
  //   {
  //     name: 'Pijpen zonder condoom',
  //     code: 'pzc_extra',
  //     id: 7
  //   },
  //   {
  //     name: 'Tussen borsten (russisch)',
  //     code: 'tbr_extra',
  //     id: 8
  //   },
  //   {
  //     name: 'BDSM klant onderdanig',
  //     code: 'bko_extra',
  //     id: 9
  //   },
  //   {
  //     name: 'Trio m/m',
  //     code: 'tmm_extra',
  //     id: 10
  //   },
  //   {
  //     name: 'Trio m/v',
  //     code: 'tmv_extra',
  //     id: 11
  //   },
  //   {
  //     name: 'Zoenen',
  //     code: 'zoe_extra',
  //     id: 12
  //   },
  //   {
  //     name: 'Massage met hoogtepunt',
  //     code: 'mmh_extra',
  //     id: 13
  //   },
  //   {
  //     name: 'Beffen',
  //     code: 'bef_extra',
  //     id: 14
  //   },
  //   {
  //     name: 'Intiem',
  //     code: 'int_extra',
  //     id: 15
  //   },
  //   {
  //     name: 'Kontlikken bij klant',
  //     code: 'kbk_extra',
  //     id: 16
  //   },
  //   {
  //     name: 'Sex zonder condoom',
  //     code: 'szc_extra',
  //     id: 17
  //   },
  //   {
  //     name: 'BDSM klant dominant',
  //     code: 'bkd_extra',
  //     id: 18
  //   },
  //   {
  //     name: 'Highclass companion',
  //     code: 'hic_extra',
  //     id: 19
  //   },
  //   {
  //     name: 'Soft SM',
  //     code: 'ssm_extra',
  //     id: 20
  //   },
  //   {
  //     name: 'Pijpen',
  //     code: 'pij_extra',
  //     id: 21
  //   },
  //   {
  //     name: 'Speciale kledingverzoeken',
  //     code: 'spk_extra',
  //     id: 22
  //   },
  //   {
  //     name: 'Tongzoenen',
  //     code: 'ton_extra',
  //     id: 23
  //   },
  //   {
  //     name: 'Plassex',
  //     code: 'pla_extra',
  //     id: 24
  //   },
  //   {
  //     name: 'Prostaat massage',
  //     code: 'prm_extra',
  //     id: 25
  //   },
  //   {
  //     name: 'Vingeren',
  //     code: 'vin_extra',
  //     id: 26
  //   },
  //   {
  //     name: 'Anaal',
  //     code: 'ana_extra',
  //     id: 27
  //   },
  //   {
  //     name: 'Pijpen deepthroat',
  //     code: 'pdt_extra',
  //     id: 28
  //   },
  //   {
  //     name: 'Squirting',
  //     code: 'squ_extra',
  //     id: 29
  //   },
  //   {
  //     name: 'Fisting',
  //     code: 'fis_extra',
  //     id: 30
  //   },
  //   {
  //     name: 'Fisting bij klant',
  //     code: 'fbk_extra',
  //     id: 31
  //   },
  //   {
  //     name: 'Aftrekken',
  //     code: 'aft_extra',
  //     id: 31
  //   },
  //   {
  //     name: 'Penismassage',
  //     code: 'pem_extra',
  //     id: 33
  //   },
  //   {
  //     name: 'Ass worship',
  //     code: 'asw_extra',
  //     id: 34
  //   },
  //   {
  //     name: 'Bondage',
  //     code: 'bon_extra',
  //     id: 35
  //   },
  //   {
  //     name: 'Boot / High heel worship',
  //     code: 'bhh_extra',
  //     id: 36
  //   },
  //   {
  //     name: 'Breath play',
  //     code: 'brp_extra',
  //     id: 37
  //   },
  //   {
  //     name: 'Caning',
  //     code: 'can_extra',
  //     id: 38
  //   },
  //   {
  //     name: 'Cock and Balls',
  //     code: 'cab_extra',
  //     id: 39
  //   },
  //   {
  //     name: 'Electro / Violet wand',
  //     code: 'evw_extra',
  //     id: 40
  //   },
  //   {
  //     name: 'Gagging / Gags',
  //     code: 'gag_extra',
  //     id: 41
  //   },
  //   {
  //     name: 'Golden shower',
  //     code: 'gos_extra',
  //     id: 42
  //   },
  //   {
  //     name: 'Kuisheidsgordel',
  //     code: 'kug_extra',
  //     id: 43
  //   },
  //   {
  //     name: 'Lak / Rubber',
  //     code: 'lar_extra',
  //     id: 44
  //   },
  //   {
  //     name: 'Masks',
  //     code: 'mas_extra',
  //     id: 45
  //   },
  //   {
  //     name: 'Mummificatie',
  //     code: 'mum_extra',
  //     id: 46
  //   },
  //   {
  //     name: 'Naalden',
  //     code: 'naa_extra',
  //     id: 47
  //   },
  //   {
  //     name: 'Nipple torture',
  //     code: 'nit_extra',
  //     id: 48
  //   },
  //   {
  //     name: 'Nursing',
  //     code: 'nur_extra',
  //     id: 49
  //   },
  //   {
  //     name: 'Opsluiting',
  //     code: 'ops_extra',
  //     id: 50
  //   },
  //   {
  //     name: 'Publieke tentoonstelling',
  //     code: 'put_extra',
  //     id: 51
  //   },
  //   {
  //     name: 'Sounding',
  //     code: 'sou_extra',
  //     id: 52
  //   },
  //   {
  //     name: 'Spitting',
  //     code: 'spi_extra',
  //     id: 53
  //   },
  //   {
  //     name: 'Travestie',
  //     code: 'tra_extra',
  //     id: 54
  //   },
  //   {
  //     name: 'Verbal humiliation',
  //     code: 'veh_extra',
  //     id: 55
  //   },
  //   {
  //     name: 'Vernedering',
  //     code: 'ver_extra',
  //     id: 56
  //   },
  //   {
  //     name: 'Worstelen',
  //     code: 'wor_extra',
  //     id: 57
  //   },
  //   {
  //     name: 'Voorbinddildo sex',
  //     code: 'vos_extra',
  //     id: 58
  //   },
  //   {
  //     name: 'Video opname',
  //     code: 'vio_extra',
  //     id: 59
  //   },
  // ]

  // Prices
  prices_po = [
    {
      name: 'Vluggertje',
      code: 'qy'
    },
    {
      name: '30 minuten',
      code: 'hu'
    },
    {
      name: '45 minuten',
      code: 'dk'
    },
    {
      name: '1 uur',
      code: 'eu'
    },
    {
      name: '2 uur',
      code: 'tu'
    },
    {
      name: '4 uur',
      code: 'vu'
    }

  ]

  prices_es = [
    {
      name: '1 uur',
      code: 'eu'
    },
    {
      name: '2 uur',
      code: 'tu'
    },
    {
      name: '4 uur',
      code: 'vu'
    },
    {
      name: '8 uur',
      code: 'au'
    },
    {
      name: '12 uur',
      code: 'hd'
    },
    {
      name: '24 uur',
      code: 'wd'
    }

  ]

  // Availability
  availability = [
    {
      name: 'Maandag',
      dagDeel: ['Ochtend', 'Middag', 'Avond', 'Nacht'],
      hd: false,
      nb: false
    },
    {
      name: 'Dinsdag',
      dagDeel: ['Ochtend', 'Middag', 'Avond', 'Nacht'],
      hd: false,
      nb: false
    },
    {
      name: 'Woensdag',
      dagDeel: ['Ochtend', 'Middag', 'Avond', 'Nacht'],
      hd: false,
      nb: false
    },
    {
      name: 'Donderdag',
      dagDeel: ['Ochtend', 'Middag', 'Avond', 'Nacht'],
      hd: false,
      nb: false
    },
    {
      name: 'Vrijdag',
      dagDeel: ['Ochtend', 'Middag', 'Avond', 'Nacht'],
      hd: false,
      nb: false
    },
    {
      name: 'Zaterdag',
      dagDeel: ['Ochtend', 'Middag', 'Avond', 'Nacht'],
      hd: false,
      nb: false
    },
    {
      name: 'Zondag',
      dagDeel: ['Ochtend', 'Middag', 'Avond', 'Nacht'],
      hd: false,
      nb: false
    }
  ];
}
