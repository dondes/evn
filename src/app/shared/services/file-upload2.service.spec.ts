import { TestBed } from '@angular/core/testing';

import { FileUpload2Service } from './file-upload2.service';

describe('FileUploadService', () => {
  let service: FileUpload2Service;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(FileUpload2Service);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
