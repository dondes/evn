import { Injectable, NgZone } from '@angular/core';
import { User, AllUsers } from "../services/user";
import { AngularFireAuth } from "@angular/fire/auth";
import { AngularFirestore, AngularFirestoreDocument } from '@angular/fire/firestore';
import { Router } from "@angular/router";
import { FormControl, FormGroup } from "@angular/forms";
import { map } from "rxjs/operators";


@Injectable({
  providedIn: 'root'
})

export class AuthService {
  userData: any; // Save logged in user data

  constructor(
    public afs: AngularFirestore,   // Inject Firestore service
    public afAuth: AngularFireAuth, // Inject Firebase auth service
    public router: Router,
    public ngZone: NgZone, // NgZone service to remove outside scope warning
  ) {
    /* Saving user data in sessionStorage when 
    logged in and setting up null when logged out */
    this.afAuth.authState.subscribe(user => {
      if (user) {
        this.userData = user;

        this.afs.doc(`/users/${this.userData.uid}`).snapshotChanges().pipe(
          map(
            action => {
              return action.payload.data();
            }
          )
        ).subscribe(
          data => {
            this.userData = data;
            this.SetUserDataSignIn(data);
            return user?.updateProfile({
              displayName: this.userData.displayName,
            });

          }

        );

        sessionStorage.clear()
        sessionStorage.setItem('user', JSON.stringify(this.userData));
        JSON.parse(sessionStorage.getItem('user')!);
      } else {
        sessionStorage.clear();
        sessionStorage.getItem('user');
      }
    })
  }

  // Sign in with email/password
  async SignIn(email: string, password: string) {

    return this.afAuth.signInWithEmailAndPassword(email, password)
      .then((result) => {
        console.log(result.user?.uid);
        sessionStorage.setItem('uid', result.user?.uid!);
        sessionStorage.setItem('user', JSON.stringify(this.userData));
        sessionStorage.getItem('user');

        this.ngZone.run(() => {
          this.router.navigate(['dashboard']);
        });


        // console.log(sessionStorage)
      }).catch((error) => {
        window.alert(error.message)
      })
  }

  SignUp(email: string, password: string, test: string) {
    console.log(test);
    return this.afAuth.createUserWithEmailAndPassword(email, password)
      .then((res) => {

        this.SetUserDataSignUp(res.user, test);
        return res.user?.updateProfile({});
      })
      .then(() => {
        this.router.navigateByUrl('/dashboard');
      })
      .catch((error) => {
        window.alert('err -->' + error.message);
      });
  }

  // Send email verfificaiton when new user sign up
  SendVerificationMail() {
    return this.afAuth.currentUser.then(u => u?.sendEmailVerification)
      .then(() => {
        this.router.navigate(['verify-email-address']);
      })
  }

  // Reset Forggot password
  ForgotPassword(passwordResetEmail: any) {
    return this.afAuth.sendPasswordResetEmail(passwordResetEmail)
      .then(() => {
        window.alert('Password reset email sent, check your inbox.');
      }).catch((error) => {
        window.alert(error)
      })
  }

  // Returns true when user is logged in and email is verified
  get isLoggedIn(): boolean {
    const user: string | any = sessionStorage.getItem('user');
    // console.log(user);
    return (user && user.emailVerified !== true) ? true : false;
  }

  /* Setting up user data when sign in with username/password, 
  sign up with username/password and sign in with social auth  
  provider in Firestore database using AngularFirestore + AngularFirestoreDocument service */
  SetUserDataSignUp(user: any, accType :string) {
    const userRef: AngularFirestoreDocument<any> = this.afs.doc(`users/${user.uid}`);
    const userData: AllUsers = {
      published:false,
      uid: user.uid,
      email: user.email,
      displayName: '',
      photoURL: user.photoURL,
      emailVerified: user.emailVerified,
      mobNumber: '',
      introTitle: '',
      gender: '',
      provGem: {
        gem: '',
        prov: ''
      },
      age: 0,
      lengte: '',
      languages: {
        dui: false,
        eng: false,
        fra: false,
        ned: false,
        spa: false
      },
      Maandag: {
        bt: '',
        et: '',
        hd: false,
        nb: false
      },
      Dinsdag: {
        bt: '',
        et: '',
        hd: false,
        nb: false
      },
      Woensdag: {
        bt: '',
        et: '',
        hd: false,
        nb: false
      },
      Donderdag: {
        bt: '',
        et: '',
        hd: false,
        nb: false
      },
      Vrijdag: {
        bt: '',
        et: '',
        hd: false,
        nb: false
      },
      Zaterdag: {
        bt: '',
        et: '',
        hd: false,
        nb: false
      },
      Zondag: {
        bt: '',
        et: '',
        hd: false,
        nb: false
      },
      bodyType: '',
      breastSize: '',
      datePrefs: {
        bd: false,
        em: false,
        es: false,
        gb: false,
        po: false,
        rp: false
      },
      dateServices: {
        abk: false,
        aft: false,
        ana: false,
        asw: false,
        bef: false,
        bhh: false,
        bis: false,
        bkd: false,
        bko: false,
        bon: false,
        brp: false,
        bus: false,
        cab: false,
        can: false,
        evw: false,
        fbk: false,
        fis: false,
        gag: false,
        gan: false,
        gos: false,
        hic: false,
        int: false,
        kbk: false,
        kim: false,
        kog: false,
        kug: false,
        lar: false,
        mas: false,
        mmh: false,
        mum: false,
        naa: false,
        nit: false,
        nur: false,
        ops: false,
        pdk: false,
        pdt: false,
        pem: false,
        pij: false,
        pla: false,
        prm: false,
        put: false,
        pzc: false,
        sou: false,
        spi: false,
        spk: false,
        squ: false,
        ssm: false,
        szc: false,
        tbr: false,
        tmm: false,
        tmv: false,
        ton: false,
        tra: false,
        veh: false,
        ver: false,
        vin: false,
        vio: false,
        vos: false,
        wor: false,
        zoe: false,
      },
      dateServicesExtra: {
        abk_extra: 0, 
        aft_extra: 0, 
        ana_extra: 0, 
        asw_extra: 0, 
        bef_extra: 0, 
        bhh_extra: 0, 
        bis_extra: 0, 
        bkd_extra: 0, 
        bko_extra: 0, 
        bon_extra: 0, 
        brp_extra: 0, 
        bus_extra: 0, 
        cab_extra: 0, 
        can_extra: 0, 
        evw_extra: 0, 
        fbk_extra: 0, 
        fis_extra: 0, 
        gag_extra: 0, 
        gan_extra: 0, 
        gos_extra: 0, 
        hic_extra: 0, 
        int_extra: 0, 
        kbk_extra: 0, 
        kim_extra: 0, 
        kog_extra: 0, 
        kug_extra: 0, 
        lar_extra: 0, 
        mas_extra: 0, 
        mmh_extra: 0, 
        mum_extra: 0, 
        naa_extra: 0, 
        nit_extra: 0, 
        nur_extra: 0, 
        ops_extra: 0, 
        pdk_extra: 0, 
        pdt_extra: 0, 
        pem_extra: 0, 
        pij_extra: 0, 
        pla_extra: 0, 
        prm_extra: 0, 
        put_extra: 0, 
        pzc_extra: 0, 
        sou_extra: 0, 
        spi_extra: 0, 
        spk_extra: 0, 
        squ_extra: 0, 
        ssm_extra: 0, 
        szc_extra: 0, 
        tbr_extra: 0, 
        tmm_extra: 0, 
        tmv_extra: 0, 
        ton_extra: 0, 
        tra_extra: 0, 
        veh_extra: 0, 
        ver_extra: 0, 
        vin_extra: 0, 
        vio_extra: 0, 
        vos_extra: 0, 
        wor_extra: 0, 
        zoe_extra: 0
      },
      esPrices: {
        au: 0,
        eu: 0,
        hd: 0,
        tu: 0,
        vu: 0,
        wd: 0
      },
      hairColor: '',
      info: '',
      origin: '',
      penisSize: '',
      poPrices: {
        dk: 0,
        eu: 0,
        hu: 0,
        qy: 0,
        tu: 0,
        vu: 0
      },
      sexualPref: '',
      accType: accType,
      creationDate: Date.now(),
      updateDate: 0,
      street: '',
      houseNr: '',
      addative: '',
      zipcode: '',
      phoneNumber: '',
      parentOf: {},
      childOf: '',
      childNum: 0,
    }
    return userRef.set(userData, {
      merge: true
    })
  }

  SetUserDataSignIn(user: any) {
    const userRef: AngularFirestoreDocument<any> = this.afs.doc(`users/${user.uid}`);
    const userData: User = {
      uid: user.uid,
      email: user.email,
      displayName: user.displayName,
      photoURL: user.photoURL,
      emailVerified: user.emailVerified
    }
    return userRef.set(userData, {
      merge: true
    })
  }

  // Sign out 
  SignOut() {
    sessionStorage.clear();
    return this.afAuth.signOut().then(() => {
      // console.log(sessionStorage);
      this.router.navigate(['home']);
    })
  }

  async GetAuthUser(email: string, password: string) {

    return new Promise<any>((resolve, reject) => {
      var uid = undefined;
      this.afAuth.signInWithEmailAndPassword(email, password)
      .then((result) => {
        if (result.user) {
          uid = result.user.uid;
          return resolve(uid);
        }
        return resolve(null);
      }).catch(() => {
        return reject(null);}
      )
    })
  }

  async RemoveRegistry(email:string, password: string) {

      return new Promise<any>((resolve, reject) => {
        var uid = undefined;
        this.afAuth.signInWithEmailAndPassword(email, password)
        .then((result) => {
          if (result.user) {
            uid = result.user.uid;
            result.user.delete();
            return resolve(uid);
          }
          return resolve(null);
        }).catch(() => {
          return reject(null);}
        )
      })


    // var uid = undefined;
    // this.afAuth.signInWithEmailAndPassword(email, password)
    // .then((result) => {
    //   if (result.user) {
    //     uid = result.user.uid;
    //     result.user.delete();
    //     sessionStorage.clear();
    //     return uid;
    //   }
    //   return null;
    // }).catch((error) => {
    //   window.alert(error.message);
    //   return null;
    // })

  }

 

  //userDataForm
  form = new FormGroup({
    displayName: new FormControl(''),
    email: new FormControl(''),
    emailVerified: new FormControl(''),
    photoUrl: new FormControl('')
  })

}