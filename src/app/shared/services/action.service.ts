import { Injectable } from '@angular/core';
import { Observable, Subject, BehaviorSubject } from 'rxjs';
import { Location } from '@angular/common';
import { MatDialog, MatDialogConfig, MatDialogRef } from '@angular/material/dialog';
import { DialogConsentComponent } from '../../components/dialog-consent/dialog-consent.component';

@Injectable({
  providedIn: 'root'
})
export class ActionService {
  private subject = new Subject<any>();

  private userSource = new Subject<any>();

  

  constructor(
    private _location: Location,
    public dialog: MatDialog,
    public dialogRef: MatDialogRef<DialogConsentComponent, any>,
  ) { }

  sendClass(userName: string) {
    this.subject.next({ text: userName });
  }

  clearClass() {
    this.subject.next();
  }

  getClass(): Observable<any> {
    return this.subject.asObservable();
  }

  changeMessage(userName: string) {
    this.userSource.next(userName);
  }

  getMessage(): Observable<any> {
    return this.userSource.asObservable();
  }

  backClicked() {
    this._location.back();
  }

  openConsentDialog(event:{ target: any; }, policy: any): void {
    var target = event?.target;
    // console.log(event);
    const dialogConfig = new MatDialogConfig();
    dialogConfig.data = [];
    dialogConfig.data[0] = policy;
    dialogConfig.data[1] = target.attributes.name.value;
    dialogConfig.data[3] = {panelClass: 'custom-modalbox'};
    this.dialog.closeAll();
    this.dialogRef = this.dialog.open(DialogConsentComponent, dialogConfig);
  }
}

export const FilterData: any[] = [
  {
    gender: [
      { id: 'Female', name: 'Vrouw' },
      { id: 'Male', name: 'Man' },
      { id: 'Shemale', name: 'Shemale' },
      { id: 'Couple', name: 'Koppel' },
    ],

    origins: [
      { id: "Nederlands", name: "Nederlands" },
      { id: "Europees", name: "Europees" },
      { id: "Oost Europees", name: "Oost Europees" },
      { id: "Zuid Europees", name: "Zuid Europees" },
      { id: "Afrikaans", name: "Afrikaans" },
      { id: "Aziatisch", name: "Aziatisch" },
      { id: "Amerikaans", name: "Amerikaans" },
      { id: "Zuid Amerikaans", name: "Zuid Amerikaans" },
      { id: "Arabisch", name: "Arabisch" },
      { id: "Turks", name: "Turks" },
      { id: "Marokkaans", name: "Marokkaans" }
    ],

    hair_color: [
      { id: "Blond", name: "Blond" },
      { id: "Donkerblond", name: "Donkerblond" },
      { id: "Burnette", name: "Burnette" },
      { id: "Bruin", name: "Bruin" },
      { id: "Wit", name: "Wit" },
      { id: "Grijs", name: "Grijs" },
      { id: "Zwart", name: "Zwart" },
      { id: "Anders", name: "Anders" }
    ],

    body_type: [
      { id: "Slank", name: "Slank" },
      { id: "Atletisch", name: "Atletisch" },
      { id: "Normaal", name: "Normaal" },
      { id: "Mollig", name: "Mollig" },
      { id: "Dik", name: "Dik" }
    ],

    date_preference: [
      {
        name: 'Escort',
        code: 'es',
        id: 0
      },
      {
        name: 'BDSM',
        code: 'bd',
        id: 1
      },
      {
        name: 'Erotische massage',
        code: 'em',
        id: 2
      },
      {
        name: 'Gangbang',
        code: 'gb',
        id: 3
      },
      {
        name: 'Prive ontvangst',
        code: 'po',
        id: 4
      },
      {
        name: 'Raamprostitutie',
        code: 'rp',
        id: 5
      }
    ],

    languages: [
      {
        name: 'Nederlands',
        code: 'ned',
        id: 0
      },
      {
        name: 'Engels',
        code: 'eng',
        id: 1
      },
      {
        name: 'Duits',
        code: 'dui',
        id: 2
      },
      {
        name: 'Frans',
        code: 'fra',
        id: 3
      },
      {
        name: 'Spaans',
        code: 'spa',
        id: 4
      },
    ],

    dateServices: [
      {
        name: 'Anaal bij klant',
        code: 'abk',
        id: 0
      },
      {
        name: 'Biseks',
        code: 'bis',
        id: 1
      },
      {
        name: 'Buiten seks',
        code: 'bus',
        id: 2
      },
      {
        name: 'Gangbang',
        code: 'gan',
        id: 3
      },
      {
        name: 'Klaarkomen in mond',
        code: 'kim',
        id: 4
      },
      {
        name: 'Klaarkomen op gezicht',
        code: 'kog',
        id: 5
      },
      {
        name: 'Plassex door klant',
        code: 'pdk',
        id: 6
      },
      {
        name: 'Pijpen zonder condoom',
        code: 'pzc',
        id: 7
      },
      {
        name: 'Tussen borsten (russisch)',
        code: 'tbr',
        id: 8
      },
      {
        name: 'BDSM klant onderdanig',
        code: 'bko',
        id: 9
      },
      {
        name: 'Trio m/m',
        code: 'tmm',
        id: 10
      },
      {
        name: 'Trio m/v',
        code: 'tmv',
        id: 11
      },
      {
        name: 'Zoenen',
        code: 'zoe',
        id: 12
      },
      {
        name: 'Massage met hoogtepunt',
        code: 'mmh',
        id: 13
      },
      {
        name: 'Beffen',
        code: 'bef',
        id: 14
      },
      {
        name: 'Intiem',
        code: 'int',
        id: 15
      },
      {
        name: 'Kontlikken bij klant',
        code: 'kbk',
        id: 16
      },
      {
        name: 'Sex zonder condoom',
        code: 'szc',
        id: 17
      },
      {
        name: 'BDSM klant dominant',
        code: 'bkd',
        id: 18
      },
      {
        name: 'Highclass companion',
        code: 'hic',
        id: 19
      },
      {
        name: 'Soft SM',
        code: 'ssm',
        id: 20
      },
      {
        name: 'Pijpen',
        code: 'pij',
        id: 21
      },
      {
        name: 'Speciale kledingverzoeken',
        code: 'spk',
        id: 22
      },
      {
        name: 'Tongzoenen',
        code: 'ton',
        id: 23
      },
      {
        name: 'Plassex',
        code: 'pla',
        id: 24
      },
      {
        name: 'Prostaat massage',
        code: 'prm',
        id: 25
      },
      {
        name: 'Vingeren',
        code: 'vin',
        id: 26
      },
      {
        name: 'Anaal',
        code: 'ana',
        id: 27
      },
      {
        name: 'Pijpen deepthroat',
        code: 'pdt',
        id: 28
      },
      {
        name: 'Squirting',
        code: 'squ',
        id: 29
      },
      {
        name: 'Fisting',
        code: 'fis',
        id: 30
      },
      {
        name: 'Fisting bij klant',
        code: 'fbk',
        id: 31
      },
      {
        name: 'Aftrekken',
        code: 'aft',
        id: 31
      },
      {
        name: 'Penismassage',
        code: 'pem',
        id: 33
      },
      {
        name: 'Ass worship',
        code: 'asw',
        id: 34
      },
      {
        name: 'Bondage',
        code: 'bon',
        id: 35
      },
      {
        name: 'Boot / High heel worship',
        code: 'bhh',
        id: 36
      },
      {
        name: 'Breath play',
        code: 'brp',
        id: 37
      },
      {
        name: 'Caning',
        code: 'can',
        id: 38
      },
      {
        name: 'Cock and Balls',
        code: 'cab',
        id: 39
      },
      {
        name: 'Electro / Violet wand',
        code: 'evw',
        id: 40
      },
      {
        name: 'Gagging / Gags',
        code: 'gag',
        id: 41
      },
      {
        name: 'Golden shower',
        code: 'gos',
        id: 42
      },
      {
        name: 'Kuisheidsgordel',
        code: 'kug',
        id: 43
      },
      {
        name: 'Lak / Rubber',
        code: 'lar',
        id: 44
      },
      {
        name: 'Masks',
        code: 'mas',
        id: 45
      },
      {
        name: 'Mummificatie',
        code: 'mum',
        id: 46
      },
      {
        name: 'Naalden',
        code: 'naa',
        id: 47
      },
      {
        name: 'Nipple torture',
        code: 'nit',
        id: 48
      },
      {
        name: 'Nursing',
        code: 'nur',
        id: 49
      },
      {
        name: 'Opsluiting',
        code: 'ops',
        id: 50
      },
      {
        name: 'Publieke tentoonstelling',
        code: 'put',
        id: 51
      },
      {
        name: 'Sounding',
        code: 'sou',
        id: 52
      },
      {
        name: 'Spitting',
        code: 'spi',
        id: 53
      },
      {
        name: 'Travestie',
        code: 'tra',
        id: 54
      },
      {
        name: 'Verbal humiliation',
        code: 'veh',
        id: 55
      },
      {
        name: 'Vernedering',
        code: 'ver',
        id: 56
      },
      {
        name: 'Worstelen',
        code: 'wor',
        id: 57
      },
      {
        name: 'Voorbinddildo sex',
        code: 'vos',
        id: 58
      },
      {
        name: 'Video opname',
        code: 'vio',
        id: 59
      },
    ]
  }
];