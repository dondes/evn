import { Injectable } from '@angular/core';
import { AngularFireDatabase, AngularFireList } from '@angular/fire/database';
import { AngularFireStorage } from '@angular/fire/storage';
import { UserDataService } from '../../shared/services/user-data.service';

import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';
import { FileUpload } from '../models/file-upload.model';

@Injectable({
  providedIn: 'root'
})
export class FileUploadService {
  private basePath = '/users/';

  constructor(private db: AngularFireDatabase, private storage: AngularFireStorage, public userDataService: UserDataService) { }

  pushFileToStorage(uid:string, fileUpload: FileUpload): Observable<number | undefined> {
    const filePath = `users/${uid}/${fileUpload.file.name}`;
    const storageRef = this.storage.ref(filePath);
    const uploadTask = this.storage.upload(filePath, fileUpload.file);

    uploadTask.snapshotChanges().pipe(
      finalize(() => {
        storageRef.getDownloadURL().subscribe(downloadURL => {
          fileUpload.url = downloadURL;
          fileUpload.name = fileUpload.file.name;
          this.saveFileData(uid,fileUpload);
        });
      })
    ).subscribe();

    return uploadTask.percentageChanges();
  }

  private saveFileData(uid:string,fileUpload: FileUpload): void {
    console.log(fileUpload);
    this.db.list(this.basePath+uid).push(fileUpload);
  }

  getFiles(uid:string, numberItems: number): AngularFireList<FileUpload> {
    return this.db.list(this.basePath+uid, ref =>
      ref.limitToLast(numberItems));
  }

  deleteFile(uid:string, fileUpload: FileUpload): void {
    this.deleteFileDatabase(uid,fileUpload.key)
      .then(() => {
        this.deleteFileStorage(uid,fileUpload.name);
      })
      .catch(error => console.log(error));
  }

  private deleteFileDatabase(uid: string,key: string): Promise<void> {
    return this.db.list(this.basePath+uid).remove(key);
  }

  private deleteFileStorage(uid:string,name: string): void {
    const storageRef = this.storage.ref(this.basePath+uid);
    storageRef.child(name).delete();
  }

  deleteStorage(uid:string): void {
    const storageRef = this.db.database.ref().child(this.basePath+uid);
    storageRef.remove();
  }
}
