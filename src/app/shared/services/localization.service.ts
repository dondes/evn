import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
    providedIn: 'root'
})

export class LocalizationService {
    private currentLanguage: string;
    private languageSubject: Subject<string> = new Subject<string>();

    constructor() {
        this.currentLanguage = 'nl';
    }

    getLanguage(): string {
        return this.currentLanguage;
    }

    setLanguage(language:string): void {
        console.log(language);
        this.currentLanguage = language;
        this.languageSubject.next(language);
    }

    getLanguageChangeObservable(): Subject<string> {
        return this.languageSubject;
    }

    translate(key:string): string {
        return $localize`:${key}`;
    }
}