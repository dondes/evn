// modules
import { AngularMaterialModule } from './angular-material.module';
import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from './app-routing.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxPaginationModule } from 'ngx-pagination';
import { NgxEditorModule } from 'ngx-editor';

// Firebase services + enviorment module
import { AngularFireModule } from "@angular/fire";
import { AngularFireAuthModule } from "@angular/fire/auth";
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireDatabaseModule } from '@angular/fire/database';
import { AngularFireStorageModule } from "@angular/fire/storage";
import firebase from 'firebase';

// Environments
import { environment } from '../environments/environment';

// Auth services
import { AuthService } from "./shared/services/auth.service";

//user-Data services
import { UserDataService } from "./shared/services/user-data.service";

// Action services
import { ActionService } from './shared/services/action.service';

// Video services
import { VideoProcessingService } from './shared/services/video-processing-services';

// Components
import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import { MainNavbarComponent } from './components/main-navbar/main-navbar.component';
import { SigninComponent } from './components/signin/signin.component';
import { RegisterComponent } from './components/register/register.component';
import { DeRegisterComponent } from './components/deregister/deregister.component';
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { ForgotPasswordComponent } from './components/forgot-password/forgot-password.component';
import { VerifyEmailComponent } from './components/verify-email/verify-email.component';
import { FooterComponent } from './components/footer/footer.component';
import { FilterOneComponent } from './components/filter-one/filter-one.component';
import { UserDetailsComponent } from './components/user-details/user-details.component';
import { UploadFormComponent } from './components/upload-form/upload-form.component';
import { UploadListComponent } from './components/upload-list/upload-list.component';
import { UploadDetailsComponent } from './components/upload-details/upload-details.component';
import { UploadForm2Component } from './components/upload-form2/upload-form2.component';
import { UploadList2Component } from './components/upload-list2/upload-list2.component';
import { UploadDetails2Component } from './components/upload-details2/upload-details2.component';
import { DialogBodyComponent } from './components/dialog-body/dialog-body.component';
import { PaginationComponent } from './components/pagination/pagination.component';
import { AdvertiserFormComponent } from './components/advertiser-form/advertiser-form.component';
import { VisitorFormComponent } from './components/visitor-form/visitor-form.component';
import { CompanyFormComponent } from './components/company-form/company-form.component';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { SearchFilterPipe } from './shared/pipes/search-filter.pipe';
import { FilterPipePipe } from './shared/pipes/filter-pipe.pipe';
import { IsLoadingPipe } from './shared/pipes/is-loading.pipe';
// import { ReviewFormComponent } from './components/review-form/review-form.component';
import { EmpolyeeFormComponent } from './components/empolyee-form/empolyee-form.component';
import { MatDialogRef } from '@angular/material/dialog';
import { EmployeeDataFormComponent } from './components/employee-data-form/employee-data-form.component';
import { EmployeeDetailsComponent } from './components/employee-details/employee-details.component';
import { TermsAndConditionsComponent } from './components/terms-and-conditions/terms-and-conditions.component';
import { PrivacyPolicyComponent } from './components/privacy-policy/privacy-policy.component';
import { CookiesStatementComponent } from './components/cookies-statement/cookies-statement.component';
import { DialogConsentComponent } from './components/dialog-consent/dialog-consent.component';




firebase.initializeApp(environment.firebase);

@NgModule({
  declarations: [
    AppComponent,
    SigninComponent,
    RegisterComponent,
    DeRegisterComponent,
    MainNavbarComponent,
    HomeComponent,
    PageNotFoundComponent,
    DashboardComponent,
    ForgotPasswordComponent,
    VerifyEmailComponent,
    FooterComponent,
    FilterOneComponent,
    UserDetailsComponent,
    UploadFormComponent,
    UploadListComponent,
    UploadDetailsComponent,
    UploadForm2Component,
    UploadList2Component,
    UploadDetails2Component,
    DialogBodyComponent,
    PaginationComponent,
    AdvertiserFormComponent,
    VisitorFormComponent,
    CompanyFormComponent,
    SearchFilterPipe,
    FilterPipePipe,
    IsLoadingPipe,
    // ReviewFormComponent,
    EmpolyeeFormComponent,
    EmployeeDataFormComponent,
    EmployeeDetailsComponent,
    TermsAndConditionsComponent,
    PrivacyPolicyComponent,
    CookiesStatementComponent,
    DialogConsentComponent,
  ],
  imports: [
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireAuthModule,
    AngularFirestoreModule,
    AngularFireDatabaseModule,
    AngularFireStorageModule,
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    FlexLayoutModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    NgxPaginationModule,
    NgxEditorModule,
    AngularMaterialModule,
  ],
  providers: [
    AuthService,
    ActionService,
    UserDataService, 
    ReactiveFormsModule,
    VideoProcessingService,
    {
      provide: MatDialogRef,
      useValue: {}
    },
  ],
  bootstrap: [AppComponent],
  entryComponents: [DialogBodyComponent, DialogConsentComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})

export class AppModule { }
